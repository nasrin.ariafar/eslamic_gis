<?php
error_reporting(E_ALL);
ini_set('display_errors', true);

set_time_limit(0);
ini_set('memory_limit', -1);

define('DS', DIRECTORY_SEPARATOR);
define('PS', PATH_SEPARATOR);

define('TEA_ROOT_DIR', dirname(__FILE__) . DS .  '..' . DS . '..');
define('TEA_APP_DIR',  TEA_ROOT_DIR . DS . 'application');
define('TEA_LIB_DIR',  TEA_ROOT_DIR . DS . 'library');
define('TEA_TEMP_DIR', TEA_ROOT_DIR . DS . 'temp');

// Define path to application directory
defined('APPLICATION_PATH')
    || define(
        'APPLICATION_PATH',
        realpath(dirname(__FILE__) . '/../application')
    );


// Ensure library/ is on include_path
set_include_path(
    implode(
        PATH_SEPARATOR,
        array(
            realpath(APPLICATION_PATH . '/../library'),
            get_include_path(),
        )
    )
);

/* check for app environment config */
$i = array_search('-e', $_SERVER['argv']);
if (!$i) {
    $i = array_search('--environment', $_SERVER['argv']);
}
if ($i) {
    define('APPLICATION_ENV', $_SERVER['argv'][$i+1]);
}
if (!defined('APPLICATION_ENV')) {
    if (getenv('APPLICATION_ENV')) {
        $env = getenv('APPLICATION_ENV');
    } else {
        $env = 'production';
    }
    define('APPLICATION_ENV', $env);
}

/** Zend_Application */
require_once 'Zend/Application.php';

/* Create application, bootstrap, and run */
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->setBootstrap(APPLICATION_PATH . '/BootstrapCli.php', 'BootstrapCli');
$application->bootstrap();
$application->run();

?>
