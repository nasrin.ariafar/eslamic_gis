define([
    'jquery',
    'underscore',
    'backbone',
    ], function ($, _, Backbone) {
        var FilterModel =  Backbone.Model.extend({
            defaults: {
                limit: 10,
                start: 0,
                deletedImage : 0,
                deleted      : 0
            },

            reset : function(){
                this.clear({
                    silent : true
                }).set(this.defaults,{
                    silent : true
                });
            }
           
        });
        return FilterModel;
    });


