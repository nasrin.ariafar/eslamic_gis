define([
    'jquery',
    'frontend/models/Base'
    ], function ($, BaseModel) {

        var UserModel = BaseModel.extend({

            defaults : {
                'id' : null,
                'username' : null,
                'firstName' : null,
                'lastName' : null,
                'password' : null,
                'email' : null,
//                'readPermission' : null,
//                'wirtePermission' : null,
                'headeId' : null,
                'photo' : null,
                'creationDate' : null,
                'updateDate' : null,
                'deleted' : 0,
                'tell' : null,
//                'counytries' : [],
//                'placeTypes' : []
            },

            url : function() {
                return '/api/users/' + ( this.get('id') || '' );
            }

        });

        return UserModel;
    });
