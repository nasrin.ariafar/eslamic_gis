define([
    'jquery',
    'frontend/models/Base'
    ], function ($, BaseModel) {

        var PlaceTypeModel = BaseModel.extend({

            defaults : {
                id      : null,
                name    : null,
                icon    : null,
                order   : null
            },

            url : function() {
                return '/api/placeTypes/' + (this.get("id") || '');
            }

        });

        return PlaceTypeModel;
    });
