define([
    'jquery',
    'frontend/models/Base'
    ], function ($, BaseModel) {

        var RegionModel = BaseModel.extend({

            url : function() {
                return '/api/regions/' + ( this.get('id') || '');
            }

        });

        return RegionModel;
    });
