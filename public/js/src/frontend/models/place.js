define([
    'jquery',
    'frontend/models/Base'
    ], function ($, BaseModel) {

        var PlaceModel = BaseModel.extend({

            defaults : {
                'id' : null,
                'name' : null,
                'localName' : null,
                'description' : null,
                'countryId' : null,
                'country' : null,
                'placeType' : null,
                'typeId' : null,
                'latitude' : null,
                'longitude' : null,
                'photo' : null,
                'email' : null,
                'website' : null,
                'tell' : null,
                'status' : 1,
                'isPublic' : null,
                'createBy' : null,
                'acceptBy' : null,
                'managerName' : null,
                'creationDate' : null,
                'updateDate' : null,
                'deleted' : 0
            },
            
            url : function() {
                return '/api/places/' + this.get('id');
            }

        });

        return PlaceModel;
    });
