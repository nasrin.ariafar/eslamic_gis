define([
    'jquery',
    'frontend/models/Base'
    ], function ($, BaseModel) {

        var AreaModel = BaseModel.extend({

            url : function() {
                return '/api/regions/' + this.get('regionId') + '/areas/' + this.get("areaId") + '/countries/' + (this.get("id") || '');
            }

        });

        return AreaModel;
    });
