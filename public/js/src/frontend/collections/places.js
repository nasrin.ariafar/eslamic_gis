define([
    'backbone',
    'frontend/collections/base',
    'frontend/models/place',
    ], function(Backbone, BaseCollection, PlaceModel) {

        var PlacesCollection = BaseCollection.extend({
            
            model : PlaceModel,
            
            url: function() {
                return 'api/places/';
            }

        
        });

        return PlacesCollection;
    });
