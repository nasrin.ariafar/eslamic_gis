define([
    'backbone',
    'frontend/collections/base',
    'frontend/models/place_type',
    ], function(Backbone, BaseCollection, PlaceTypeModel) {

        var PlaceTypesCollection = BaseCollection.extend({
            
            model : PlaceTypeModel,

            comparator  :  function(item) {
                return parseInt(item.get("order"));
            },
              
            url: function() {
                return 'api/placeTypes'
            }

        
        });

        return PlaceTypesCollection;
    });
