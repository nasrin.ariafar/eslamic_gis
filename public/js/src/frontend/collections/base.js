define([
    'jquery',
    'underscore',
    'backbone',   
    'frontend/models/FilterModel'
], function ($, _, Backbone, FilterModel) {
    var BaseCollections = Backbone.Collection.extend({
        model:  new Backbone.Model(),
	
        initialize: function(models, opt){
            this.options = opt;
            var $this = this;
                
            this.filters = new FilterModel();

            this.filters.on('change', function() {
                $this.reset()
                $this.fetch({
                    add  : true
                });
            });
        },
	
        parse: function( resp ){
            if(resp && this.filters){
                this.count = resp.count ;
            }
            return resp.items;
        },
	
        setFilters: function(filters){
            var limit = this.filters.get('limit');
            var start = filters.start ;
        
            this.filters.clear({
                silent : true
            })
            filters.limit = limit;
            filters.start = start;
            this.filters.set(filters);
        },
	
        increase: function(){
            this.filters.set('start', this.filters.get('start') + (count ? count : this.filters.get('limit')));
            return this;
        },

        nextPage : function(count) {
            var start = this.filters.get('start') + (count ? count : this.filters.get('limit'));
            this.filters.set({
                start : start
            }, {
                silent : true
            });
            return this;
        },
    
        getOpt: function(){
            return this.options;
        },

        fetch: function(options) {

            options || (options = {});

            var collection = this,
            success = options.success;

            options || (options = {});

            // this extention done for new updates of backbone
            $.extend(options, {
                update  : true,
                remove  : false,
                silent  : true
            });
                
            if (this.filters) {
                var filters = this.filters.toJSON();
                if (options.data) {
                    for (var i in filters) {
                        options.data[i] = filters[i];
                    }
                }
                else {
                    options.data = filters;
                }
            }

            options.beforeSend = this.setHeader;

            options.success = function(collection, resp, xhr) {
                collection.trigger('fetchSuccess', resp, collection);
                if (success)
                    success(collection, resp);

            }

            options.error = function(collection, resp, xhr){
                collection.trigger('fetchError', resp, collection);
            }

            return Backbone.Collection.prototype.fetch.call(this, options);
        }
		
    });

    return BaseCollections;
});
