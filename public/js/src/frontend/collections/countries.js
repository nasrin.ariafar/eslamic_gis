define([
    'backbone',
    'frontend/collections/base',
    'frontend/models/country',
    ], function(Backbone, BaseCollection, CountryModel) {

        var CountriesCollection = BaseCollection.extend({
            
            model : CountryModel,
            
            url: function() {
                return this.options.url ||  'api/countries/'
            }
        
        });

        return CountriesCollection;
    });
