define([
    'jquery',
    'backbone',
    'underscore',
    'bootstrap',
    'select_picker',
    'bootbox'
    ], function($, Backbone, _) {
        window._views = [];

        var BaseView = Backbone.View.extend({
            views : [],

            constructor : function(options){
                this.options = options;

                Backbone.View.apply(this, arguments);
                this.$el.data('removable', true);
                window._views.push(this.$el);
                this.views.push(this);
            },

            setRemovable : function(isRemovable){
                this.isRemovable = isRemovable;
            },

            dispose: function() {
                this.unbind();
                this.remove();
            }

        });

        _.mixin({
            ucFirst : function(str) {
                if (typeof str != "string") {
                    return str;
                }
                return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
            },
            
            translate : function convert(string, seperate) {
                
                if(string && string != "NULL"){
                    var string = string.toString();

                    if(seperate){
                        while (/(\d+)(\d{3})/.test(string.toString())){
                            string = string.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                        }
                    }

                    if(string){
                        var persian_numbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                        string = string.replace(/0/g, persian_numbers[0]);
                        string = string.replace(/1/g, persian_numbers[1]);
                        string = string.replace(/2/g, persian_numbers[2]);
                        string = string.replace(/3/g, persian_numbers[3]);
                        string = string.replace(/4/g, persian_numbers[4]);
                        string = string.replace(/5/g, persian_numbers[5]);
                        string = string.replace(/6/g, persian_numbers[6]);
                        string = string.replace(/7/g, persian_numbers[7]);
                        string = string.replace(/8/g, persian_numbers[8]);
                        string = string.replace(/9/g, persian_numbers[9]);
                    }
                    return string;
                }
                else return "-";
            }

        });

        return BaseView;
    });
