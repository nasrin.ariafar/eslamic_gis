define([
    'jquery',
    'backbone',
    'underscore',
    'frontend/views/base',
    'text!frontend/templates/change_password.html',
    'frontend/models/user',
    'i18n!nls/labels',
    
    ], function ($, Backbone, _, BaseView, changePasswordTpl, UserModel, labels) {

        var editProfile = BaseView.extend({
            
            className : 'modal-dialog',
                   
            events : {
                'click  #save-detail'                  : 'saveDetail',
                'click  #save-password'                : 'changePassword'
            },
                    
            initialize : function(opt) {
                this.template = _.template(changePasswordTpl);
                this.render();
            },
            
            render : function() {
                this.$el.html(this.template({
                    labels : labels
                }));

                return this;
            },

            checkPassword : function(e){
                if($("#new_password", this.$el).val().length > 5){
                //                    $("#new_password", this.$el).next().html('<span style="color:green;" class="voo-checkmark-2"></span>');
                } else {
                    $(".extra-chars").show();
                }
            },
            
            passwordComplexify : function(){
                var $this = this;
                $("#new_password" , $this.$el).complexify({}, function (valid, complexity) {
                    if (!valid) {
                        $('#progress',$this.$el).css({
                            'width':complexity + '%'
                        }).removeClass('progressbarValid').addClass('progressbarInvalid');
                    } else {
                        $('#progress' ,$this.$el).css({
                            'width':complexity + '%'
                        }).removeClass('progressbarInvalid').addClass('progressbarValid');
                    }
                });
            },
            
            changePassword : function(e){
                var $this = this;
                var password = $('#change-password-form', this.$el).serializeJSON();
                var valid = true;
                $(".error").hide();
                
                if($("#old_password").val() == ""){
                    $(".einter-oldpass").show();
                    valid = false;
                    return;
                }else if(password.newPass.length < 5){
                    $(".extra-chars").show();
                    valid = false;
                    return;
                }else if(password.newPass != password.confirm){
                    $(".match-passes").show();
                    valid = false;
                    return;
                }

                if(!valid)
                    return;
                else{
                    $(".error").hide();
                    var userModel = new UserModel();
                    userModel.set( window.currentUser.toJSON());

                    userModel.save({
                        'password'  : password.newPass,
                        'old'       : password.old
                    },{
                        success : function(resp){
                            $this.render();
                            $(".save-successfull-msg").show();
                            setTimeout(function(){
                                $this.$el.parents(".modal").modal("hide")
                            }, 2000);
                          
                        },
                        error : function(model ,error){
                            if(error.status == 401){
                                $(".wrong-oldpass").show();
                            }
                        }
                    })
                }
            }
        });

        return editProfile;
    });