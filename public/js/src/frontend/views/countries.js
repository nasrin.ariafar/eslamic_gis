define([
    'app',
    'underscore',
    'backbone',
    'frontend/views/base_list',
    'text!frontend/templates/country/list.html',
    'text!frontend/templates/country/item.html',
    'text!frontend/templates/country/form.html',
    'frontend/views/areas',
    'i18n!nls/labels',
], function(App, _, Backbone, BaseListCmp, ListTpl, ItemTpl, FormTpl, CountriesView, Labels) {

    var CountriesListView = BaseListCmp.extend({
        fetchCollection: false,
        events: {
            'click .add-country': 'addNewItem',
            'click .delete-country': 'deleteAction',
            'click .edit-country': 'showForm',
            'click .update-country': 'update'
        },
        initialize: function(options) {
            this.template = _.template(ListTpl);
            this.itemTpl = _.template(ItemTpl);

            this.region = this.options.region;
            this.area = this.options.area;

            CountriesListView.__super__.initialize.call(this, options);
        },
        render: function() {
            this.$el.html(this.template({
                labels: Labels,
                area: this.area.toJSON(),
                region: this.region.toJSON()
            }));
        },
        addNewItem: function() {
            var obj = $("form", this.$el).serializeJSON(),
                    new_item = new this.collection.model({
                        areaId: this.area.get("id"),
                        regionId: this.region.get("id")
                    }),
                    $this = this;

            new_item.save(obj, {
                success: function(model) {
                    $("input[name=name]").val("");
                    $this.collection.add(model);
                }
            });
        },
        deleteItem: function(e) {
            var el = $(e.target).closest(".row"),
                    countyId = el.data("country"),
                    country = this.collection.get(countyId);

            country.destroy({
                wait: true,
                success: function() {
                    el.remove();
                },
                error: function(error) {
                    App.Notify('error', Labels.forbidden_delete, '', {
                        addclass: 'stack-bottomleft custom',
                        stack: "stack_bottomleft",
                        icon: 'picon picon-32 picon-fill-color',
                        opacity: .8,
                        nonblock: {
                            nonblock: true
                        }
                    });
                }
            });
        },
        showForm: function(e) {
            var el = $(e.target).closest(".row"),
                    id = el ? el.data("country") : null,
                    model = this.collection.get(id);

            model && this.popForm(model);
        },
        popForm: function(model) {
            var $this = this;

            var tpl = _.template(FormTpl);

            $("#popup-holder", this.$el).html(tpl(_.extend(model.toJSON(), {
                labels: Labels
            })));

            $(".modal", this.$el).modal("show");

            $("#popup-holder", this.$el).modal({
                backdrop: "static"
            });
        },
        update: function(e) {
            var id = $(e.target).data("id"),
                    model = this.collection.get(id),
                    $this = this;

            if (model.get("name") != $(".modal [name=name]", this.$el).val()) {
                model.save({
                    name: $(".modal [name=name]", this.$el).val()
                }, {
                    success: function() {
                        $(".modal", $this.$el).modal("hide");
                        var holder = $("[data-country=" + model.get("id") + "]");
                        $("span.name", holder).html(model.get("name"))
                    }
                })
            }
        }



    });
    return CountriesListView
});