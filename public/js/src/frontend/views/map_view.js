
define([
    'app',
    'underscore',
    'backbone',
    'frontend/views/base_list',
    'frontend/collections/place_types',
    'text!frontend/templates/mapView/map_view.html',
    'text!frontend/templates/mapView/filter.html',
    'text!frontend/templates/mapView/place_type_item.html',
    'text!frontend/templates/mapView/infobox.html',
    'text!frontend/templates/mapView/place_info.html',
    'frontend/collections/regions',
    'frontend/collections/places',
    'frontend/views/regions_selection',
    'i18n!nls/labels',
    'select_picker'
], function(App, _, Backbone, BaseList, PlaceTypesCollection, Tpl, FilterTpl, PlaceTypeItem, InfoBoxTpl, PlaceInfoTpl, RegionsCollection, PlacesCollection, RegionsSelectionView, Labels) {

    var MapView = BaseList.extend({
        className: "map-view",
        fetchCollection: false,
        events: {
            'click .search-by-name': 'searchByName',
            'keyup .search-by-name input': 'toggleSearchNameBtn',
            'click .search-by-regions-types': 'searchByCountriesType',
            'click .close-info-window': 'closeInfoWindow',
            'change #change-map-type': 'changeMapType'
        },
        initialize: function(options) {

            _.bindAll(this, "toggleBtn");

            this.template = _.template(Tpl);
            this.markers = [];
            this.mapType = "google-map";
            this.collection = new PlacesCollection([]);
            this.collection.on('fetchSuccess', this.onFetchSuccess, this);
            this.collection.on('add', this.addMarker, this);
            this.collection.on('reset', this.reset, this);

            this.regionsCollection = new RegionsCollection([]);
            this.regionsCollection.on("fetchSuccess", this.renderRegionsFilter, this);
            this.regionsCollection.fetch();

            if (window.currentUser.get("type") == 1) {
                this.placeTypesCollection = new PlaceTypesCollection([]);
                this.placeTypesCollection.on("fetchSuccess", this.renderPlaceTypesFilter, this);
                this.placeTypesCollection.fetch();
            } else {
                var $this = this;
                this.placeTypesCollection = new PlaceTypesCollection([]);
                this.placeTypesCollection.fetch({
                    async: false
                });

                window.currentUser.fetch({
                    success: function(resp) {
                        var types = [];
                        _.each(window.currentUser.get("placeTypes"), function(item) {
                            types.push($this.placeTypesCollection.get(item.placeTypeId))
                        });

                        $this.placeTypesCollection = new PlaceTypesCollection(types);
                        $this.renderPlaceTypesFilter();
                    }
                })
            }


            this.render();

            return this;
        },
        onFetchSuccess: function(resp, collection) {

            if (!collection.length) {
                App.Notify('info', Labels.no_items, '', {
                    addclass: 'stack-bottomleft custom',
                    stack: "stack_bottomleft",
                    icon: 'picon picon-32 picon-fill-color',
                    opacity: .8,
                    nonblock: {
                        nonblock: true
                    }
                });
            }
            else {
                var str = _.translate(resp.count) + "مورد یافت شد.‏ ";
                App.Notify('info', str, '', {
                    addclass: 'stack-bottomleft custom',
                    stack: "stack_bottomleft",
                    icon: 'picon picon-32 picon-fill-color',
                    opacity: .8,
                    nonblock: {
                        nonblock: true
                    }
                });
            }
            this.addRows();
        },
        changeMapType: function(e) {
            this.mapType = $(e.target).val();
            this.Bmap && this.Bmap.dispose();
            $("#map-canvas").replaceWith($('<div id="map-canvas"></div>'))
            this.initMap();
            this.addRows();
        },
        addRows: function() {
            var $this = this;

            this.collection.each(function(model) {
                if ($this.mapType == "google-map")
                    $this.addMarker(model);
                else
                    $this.addPushpin(model);
            });
        },
        reset: function() {

            this.collection.filters.clear({
                silent: true
            });

            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }

            this.closeInfoWindow();
            this.Bmap && this.Bmap.entities.clear();
            this.markers = [];
        },
        addPushpin: function(model) {
            var $this = this;
            var position = new Microsoft.Maps.Location(model.get("latitude"), model.get("longitude"));
            var pushpinOptions = {
                icon: model.get("placeType").icon,
                width: 80,
                height: 100,
                visible: true
            };

            var pushpin = new Microsoft.Maps.Pushpin(position, pushpinOptions);
            var pushpinClick = Microsoft.Maps.Events.addHandler(pushpin, 'click', function(e) {
                $this.showPlaceInfo(model);
                //                    var info_tpl = _.template(InfoBoxTpl);
                //                    var infoboxOptions = {
                //                        title           : model.get("name"),
                //                        description: info_tpl(_.extend(model.toJSON(),{
                //                            labels : Labels
                //                        })),
                //                        visible: true,
                //                        location        : e.target.location
                //                    };
                //                    var defaultInfobox = new Microsoft.Maps.Infobox(position, infoboxOptions );
                //                    $this.Bmap.entities.push(defaultInfobox);
            });
            this.Bmap.setView({
                center: position,
                zoom: 8
            });

            this.Bmap.entities.push(pushpin);
        },
        addMarker: function(model) {
            var $this = this;
            var icon = new google.maps.MarkerImage(
                    model.get("placeType").icon //url
                    //                    new google.maps.Size(30, 30) //size
                    )

            var posision = new google.maps.LatLng(model.get("latitude"), model.get("longitude"));

            var marker = new google.maps.Marker({
                position: posision,
                map: this.Gmap,
                icon: icon
            });

            google.maps.event.addListener(marker, 'click', function() {
                $this.Gmap.setCenter(marker.getPosition());
                $this.Gmap.setZoom(12);
                //
                //                    var populationOptions = {
                //                        strokeColor: '#FF0000',
                //                        strokeOpacity: 0.8,
                //                        strokeWeight: 2,
                //                        fillColor: '#FF0000',
                //                        fillOpacity: 0.35,
                //                        map: $this.Gmap,
                //                        center:  new google.maps.LatLng(model.get("latitude"), model.get("longitude")),
                //                        radius: 200
                //                    };
                //                    // Add the circle for this city to the map.
                //                    $this.cyrcle = new google.maps.Circle(populationOptions);
                //
                //                    //show info window
                //
                //                    var info_tpl = _.template(InfoBoxTpl);
                //                    var infowindow = new google.maps.InfoWindow({
                //                        content: info_tpl(_.extend(model.toJSON(),{
                //                            title           : model.get("name"),
                //                            labels : Labels
                //                        }))
                //                    });
                //
                //                    infowindow.open($this.Gmap, marker);


                $this.showPlaceInfo(model);
            });


            this.markers.push(marker);

        },
        getInfoBoxHtml: function(model) {
            var info_tpl = _.template(InfoBoxTpl);
            return (info_tpl(_.extend(model.toJSON(), {
                labels: Labels
            })));
        },
        showPlaceInfo: function(model) {

            var info_tpl = _.template(PlaceInfoTpl);
            $(".place-info").html(info_tpl(_.extend(model.toJSON(), {
                labels: Labels
            })));

            $(".place-info").show();
        },
        closeInfoWindow: function() {
            $(".place-info").empty();
            $(".place-info").hide();

            if (this.cyrcle) {
                this.cyrcle.setMap(null);
                delete this.cyrcle;
            }

            this.mapType == "google-map" && this.Gmap.setZoom(2);
            this.mapType == "bing-map" && this.Bmap.setView({
                zoom: 2
            });

        },
        afterAddRows: function() {
            google.maps.event.addDomListener(window, 'load');
        },
        render: function() {

            var filter_tpl = _.template(FilterTpl);
            $("#visitor-menu").html(filter_tpl({
                labels: Labels
            }));

            $("#page-wrapper").html(this.template({
                labels: Labels
            }));

        },
        initMap: function(mapType) {

            if (this.mapType == "google-map") {
                this.initGoogleMap();

            } else {
                this.initBingMap();
            }

        },
        initGoogleMap: function() {
            var mapOptions = {
                zoom: 2,
                center: new google.maps.LatLng(0, 0),
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.BOTTOM_CENTER
                },
                panControl: true,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                scaleControl: true, // fixed to BOTTOM_RIGHT
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.LEFT_TOP
                }

            };


            this.Gmap = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);
        },
        initBingMap: function() {

            this.Bmap = new Microsoft.Maps.Map(document.getElementById('map-canvas'), {
                credentials: 'ApvCCJDlR4OnZnk97xg4fNYsopreV6ujjn8t7G1_v68WihTAhx-m_bQ95xga8Yu_',
                showMapTypeSelector: false,
                showBreadcrumb: true,
                zoom: 2
            });

        },
        renderRegionsFilter: function() {

            this.regionsSelectionView = new RegionsSelectionView({
                collection: this.regionsCollection,
                checked: true,
                onSelectCountry: this.toggleBtn
            });

            $(".regions-list").html(this.regionsSelectionView.$el);

            this.regionsSelectionView.registerEvents();

        },
        toggleBtn: function() {

            if ($(".regions-list input:checked").length == 0 && $(".place-types-list input:checked").length == 0) {
                $(".search-by-regions-types").addClass("disabled");
            }
            else {
                $(".search-by-regions-types").removeClass("disabled");
            }
        },
        toggleSearchNameBtn: function(e) {
            if ($(e.target).val().trim() == "") {
                $(".search-by-name").addClass("disabled");
                return;
            }
            else {
                $(".search-by-name").removeClass("disabled");
                e.keyCode == 13 && this.searchByName(e);
            }
        },
        renderPlaceTypesFilter: function() {
            var $this = this;

            this.placeTypesCollection.each(function(type) {
                var tpl = _.template(PlaceTypeItem);
                $(".place-types-list", $this.$el).append(tpl(type.toJSON()));
            });

            $('.place-types-list input', this.$el).iCheck({
                checkboxClass: 'icheckbox_flat-purple',
                radioClass: 'iradio_flat-purple'
            });

            $('.place-types-list input', this.$el).on('ifToggled', function() {
                $this.toggleBtn();
            });

        },
        searchByCountriesType: function(e) {

            if ($(e.target).closest(".search").hasClass("disabled"))
                return;

            this.collection.reset();
            this.selectedCountries = [];
            this.selectedPlaceTypes = [];

            var $this = this,
                    filters = {};

            $(".country-checkbox:checked", this.$el).each(function(index, el) {
                $this.selectedCountries.push($(el).data("id"));
            });
            if (this.selectedCountries.length) {
                filters.countries = this.selectedCountries.join(',');
            }

            $(".place-type-checkbox:checked", this.$el).each(function(index, el) {
                $this.selectedPlaceTypes.push($(el).data("id"));
            });
            if (this.selectedPlaceTypes.length) {
                filters.types = this.selectedPlaceTypes.join(',');
            }

            this.fetch(filters);
        },
        searchByName: function(e) {

            if ($(e.target).closest(".search").hasClass("disabled"))
                return;

            var name = $(".search-by-name input").val();
            if (name.trim() == "")
                return;

            this.collection.reset();
            this.fetch({
                "name": name
            });

        },
        fetch: function(obj) {
            var obj = obj || {},
                    filters = _.extend(obj, {
                        status: 2
                    });

            this.collection.filters.set(filters, {
                silent: true
            });

            this.collection.fetch();
        }


    });

    return MapView;
});
