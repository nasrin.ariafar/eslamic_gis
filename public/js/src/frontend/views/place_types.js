define([
    'app',
    'underscore',
    'backbone',
    'frontend/views/base_list',
    'text!frontend/templates/placeType/list.html',
    'text!frontend/templates/placeType/item.html',
    'text!frontend/templates/placeType/form.html',
    'frontend/collections/place_types',
    'frontend/views/form',
    'i18n!nls/labels',
], function(App, _, Backbone, BaseListCmp, ListTpl, ItemTpl, FormTpl, PlaceTypesCollection, FormView, Labels) {

    var RegionsListView = BaseListCmp.extend({
        fetchCollection: true,
        events: {
            'click .add-place': 'showForm',
            'click .delete': 'deleteAction',
            'click .edit': 'showForm',
            'click .create-item': 'createItem',
            'click .update-item': 'update'
        },
        initialize: function(options) {

            this.template = _.template(ListTpl);
            this.itemTpl = _.template(ItemTpl);

            this.collection = new PlaceTypesCollection([]);
            RegionsListView.__super__.initialize.call(this, options);
        },
        afterAddRows: function() {
            var $this = this;
            $("#sortable", this.$el).sortable({
                update: function() {
                    _.each($(".placeType-item", $(this)), function(el, index) {
                        var id = $(el).attr("id"),
                                order = $(el).index() + 1;

                        if (order != $(el).data("order")) {
                            var model = $this.collection.get(id);

                            model.save({
                                "order": order
                            });
                        }
                    });
                }
            });
            $("#sortable", this.$el).disableSelection();
        },
        preventDefualt: function(e) {
            if (e.keyCode == 13)
                return false;
        },
        validate: function() {

            var validate = true;

            if ($("[name=name]").val() == "")
                validate = false;

            if (!$('#file')[0].files[0])
                validate = false;

            return validate;

        },
        createItem: function() {
            this.model = new this.collection.model();
            this.submit();
        },
        update: function(e) {
            var id = $(e.target).data("id");
            this.model = this.collection.get(id);
            this.submit();
        },
        submit: function() {

            var $this = this,
                    mode = this.model.get("id") ? "edit" : "create";

            $(".error").removeClass("error");
            $(".rquired_error").css("visibility", "hidden");
            $(".error_msg").css("visibility", "hidden");

            if (mode == "create" && !this.validate())
                return;

            //                else if(mode == "edit" && this.model.)

            $(".modal-footer .btn:not(.btn-default)").attr("disabled", true);
            var data = _.extend(this.model.toJSON(), $("form", this.$el).serializeJSON(), {
                order: (this.model.get("order")) ? this.model.get("order") : $(".placeType-item", this.$el).length + 1
            }),
                    fd = new FormData();
            fd.append('file', $('#file')[0].files[0]);

            for (key in data) {
                if (data[key])
                    fd.append(key, data[key]);
            }
            debugger

            $.ajax({
                type: "POST",
                url: "/api/placeTypes",
                processData: false,
                contentType: false,
                data: fd,
                dataType: 'JSON',
                success: function(obj) {
                    if (mode == "create") {
                        $this.model.set(obj);
                        $this.collection.add($this.model);
                    } else {
                        $this.model.set(obj);
                        $this.addRow($this.model, "edit");
                    }

                    $this.hideForm();
                },
                error: function(error) {
                    console.log(error)
                }
            });

            return false;
        },
        
        deleteItem: function(e) {
            var el = $(e.target).closest("li"),
                    id = el.attr("id"),
                    placeType = this.collection.get(id);

            placeType.destroy({
                success: function(model, resp) {
                    el.remove();
                },
                error: function(model, error) {
                    App.Notify('error', Labels.forbidden_delete, '', {
                        addclass: 'stack-bottomleft custom',
                        stack: "stack_bottomleft",
                        icon: 'picon picon-32 picon-fill-color',
                        opacity: .8,
                        nonblock: {
                            nonblock: true
                        }
                    });
                }
            });
        },
        showForm: function(e) {
            var el = $(e.target).closest("li"),
                    id = el ? el.attr("id") : null,
                    model = id ? this.collection.get(id) : new this.collection.model();

            this.popForm(model);
        },
        popForm: function(model) {
            var $this = this;

            var formView = new FormView({
                model: model,
                tpl: FormTpl,
                onSaveSuccess: function(obj, mode) {
                    if (mode == "create") {
                        $this.collection.add(obj);
                    } else {
                        $this.addRow(model, "edit");
                    }

                    $this.hideForm();

                }
            });

            $("#popup-holder", this.$el).html(formView.render().$el);

            $("#popup-holder", this.$el).modal({
                backdrop: "static"
            });
        },
        hideForm: function() {
            $("#popup-holder", this.$el).modal("hide");
        }



    });
    return RegionsListView
});