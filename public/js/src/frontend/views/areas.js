define([
    'app',
    'underscore',
    'backbone',
    'frontend/views/base_list',
    'text!frontend/templates/area/list.html',
    'text!frontend/templates/area/item.html',
    'text!frontend/templates/area/form.html',
    'frontend/collections/areas',
    'frontend/views/countries',
    'frontend/collections/countries',
    'i18n!nls/labels',
], function(App, _, Backbone, BaseListCmp, ListTpl, ItemTpl, FormTpl, AreasCollection, CountriesView, CountriesCollection, Labels) {

    var AreasListView = BaseListCmp.extend({
        fetchCollection: false,
        events: {
            'click .add-area': 'addNewItem',
            'click .show-countries': 'showCountries',
            'click .delete-area': 'deleteAction',
            'click .show-areas': 'showAreas',
            'click .edit-area': 'showForm',
            'click .update-area': 'update'
        },
        initialize: function(options) {
            this.template = _.template(ListTpl);
            this.itemTpl = _.template(ItemTpl);

            this.region = this.options.region;

            AreasListView.__super__.initialize.call(this, options);
        },
        render: function() {
            this.$el.html(this.template({
                labels: Labels,
                region: this.region.toJSON()
            }));
        },
        showAreas: function() {
            this.countriesView.dispose();
            this.render();
            this.addRows();
        },
        addNewItem: function() {
            var obj = $("form", this.$el).serializeJSON(),
                    new_item = new this.collection.model({
                        regionId: this.region.get("id")
                    }),
                    $this = this;

            new_item.save(obj, {
                success: function(model) {
                    $("input[name=name]").val("");
                    $this.collection.add(model);
                }
            })

        },
        showCountries: function(e) {
            this.countriesView && this.countriesView.dispose();

            var areaId = $(e.target).closest(".row").data("area"),
                    area = this.collection.get(areaId);

            if (area) {
                if (!area.countriesCollection) {
                    area.countriesCollection = new CountriesCollection(area.get("countries") || [], {
                        areaId: area.get("id")
                    });
                }

                this.countriesView = new CountriesView({
                    area: area,
                    region: this.region,
                    collection: area.countriesCollection
                });

                this.$el.html(this.countriesView.$el);
            }
        },
        dispose: function() {
            this.countriesView && this.countriesView.dispose();
            AreasListView.__super__.dispose.call(this);
        },
        deleteItem: function(e) {
            var el = $(e.target).closest(".row"),
                    areaId = el.data("area"),
                    area = this.collection.get(areaId);

            area.destroy({
                wait: true,
                success: function() {
                    el.remove();
                },
                error: function(error) {
                    App.Notify('error', Labels.forbidden_delete, '', {
                        addclass: 'stack-bottomleft custom',
                        stack: "stack_bottomleft",
                        icon: 'picon picon-32 picon-fill-color',
                        opacity: .8,
                        nonblock: {
                            nonblock: true
                        }
                    });
                }
            });
        },
        showForm: function(e) {
            var el = $(e.target).closest(".row"),
                    id = el ? el.data("area") : null,
                    model = this.collection.get(id);

            model && this.popForm(model);
        },
        popForm: function(model) {
            var tpl = _.template(FormTpl);

            $("#popup-holder", this.$el).html(tpl(_.extend(model.toJSON(), {
                labels: Labels
            })));

            $(".modal", this.$el).modal("show");

            $("#popup-holder", this.$el).modal({
                backdrop: "static"
            });
        },
        update: function(e) {
            var id = $(e.target).data("id"),
                    model = this.collection.get(id),
                    $this = this;

            if (model.get("name") != $(".modal [name=name]", this.$el).val()) {
                model.save({
                    name: $(".modal [name=name]", this.$el).val()
                }, {
                    success: function() {
                        $(".modal", $this.$el).modal("hide");
                        var holder = $("[data-area=" + model.get("id") + "]");
                        $("span.name", holder).html(model.get("name"))
                    }
                })
            }
        }


    });
    return AreasListView
});