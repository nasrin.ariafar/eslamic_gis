
define([
    'jquery',
    'underscore',
    'backbone',
    'frontend/views/base',
    'i18n!nls/labels',
    'select_picker',
    'icheck'
    ], function ($, _, Backbone, BaseView, Labels) {

        var ItemView = BaseView.extend({
            
            className : "modal-dialog place-type-form",
            
            events : {
                'click #image_upload_btn'       : 'openSelectionFile',
                'change #file'                  : 'setImage',
                'click .create-item'            : 'submit',
                'click .update-item'            : 'submit',
                'focus .error input'            : 'removeClassError',
                'change .error select'          : 'removeClassError',
                'click .error ul'               : 'removeClassError'
            },
            
            initialize : function(options) {
                this.template = _.template(options.tpl);
            },

            render : function() {
                this.$el.html(this.template(_.extend(this.model.toJSON(), {
                    labels  : Labels
                })));
                return this;
            },
            
            showNextStep  : function(e){
                var current_step = $(e.target).parents('.modal-content');
                current_step.hide();
                current_step.next().show();
                this.afterSwitchNext && this.afterSwitchNext();
            },
            showPreviousStep : function(e){
                var current_step =  $(".modal-content:visible");
                current_step.hide();
                current_step.prev().show();
            },

            setImage : function(e){
                var input = $('#file')[0];

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('img.image-preview', this.$el).attr('src', e.target.result);
                    };
                    
                    reader.readAsDataURL(input.files[0]);
                }
            },
            
            openSelectionFile : function(){
                $("#file").trigger("click");
            },

            validate : function(){
                var validate =  true;

                $("form [required]").each(function(index, el){
                    if($(el).val() == ""){
                        validate =  false;
                        $(el).parent().addClass("error");
                    }
                });
                
                !validate && $(".rquired_error").css("visibility", "visible");
            
                return validate;
            },

            removeClassError : function(e){
                $(e.target).parents(".error").removeClass("error");
            }

            
        });

        return ItemView;
    });
