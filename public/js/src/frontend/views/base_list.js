define([
    'jquery',
    'underscore',
    'backbone',
    'frontend/views/base',
    'i18n!nls/labels',
    'bootbox',
    'bootstrap',
], function($, _, Backbone, BaseCmp, Labels, bootbox) {

    var BaseListView = BaseCmp.extend({
        events: {
            'click .submit': 'addNewItem',
            'click .load-more-items': 'loadMoreItems'
        },
        fetchCollection: true,
        initialize: function(options) {
            this.subViews = [];
            this.options = options || {};

            _.bindAll(this, 'deleteItem');

            this.collection.on('add', this.onAddCallback, this);
            this.collection.on('remove', this.onRemoveCallback, this);
            this.collection.on('reset', this.reset, this);
            this.collection.on('fetchSuccess', this.onFetchSuccess, this);
            this.collection.on('fetchError', this.onFetchError, this);

            this.render();
            this.fetchCollection ? this.fetch() : this.onFetchSuccess();

            return this;
        },
        deleteItem: function() {
        },
        deleteAction: function(e) {
            var $this = this;
            bootbox.confirm(_.extend({
                'message': "آیا از حذف مورد انتخاب شده مطمین هستید؟",
                'callback': function(result) {
                    result && $this.deleteItem(e);
                }
            }, {
                buttons: {
                    'cancel': {
                        label: __('خیر')
                    },
                    'confirm': {
                        label: _.ucFirst(__('بله'))
                    }
                }

            }));
        },
        render: function() {
            this.$el.html(this.template({
                labels: Labels
            }));
        },
        onAddCallback: function(model, index, init) {
            this.addRow(model);
        },
        onRemoveCallback: function(model) {
        },
        fetch: function() {
            this.collection.fetch();
        },
        onFetchSuccess: function(collection) {
            this.addRows(collection);
        },
        onFetchError: function(error) {
            console.log(error);
        },
        reset: function() {
            _.each(this.subViews, function(itemView) {
                itemView.dispose();
            });

            this.subViews = [];
        },
        addRows: function() {
            for (var i = this.collection.filters.get('start'); i < this.collection.models.length; i++) {
                var model = this.collection.models[i];
                this.addRow(model)
            }

            this.afterAddRows();
        },
        afterAddRows: function() {
            if (this.collection.length < this.collection.count) {
                $(".load-more-items").css('display', 'block');
                this.hasMoreItems = true;

            } else {
                $(".load-more-items").css('display', 'none');
                this.hasMoreItems = false;
            }

            $('[data-toggle="tooltip"]', this.$el).tooltip()
        },
        addRow: function(model, mode) {
            var mode = mode || "create",
                    item_el = this.itemTpl(_.extend(model.toJSON(), {
                        labels: Labels
                    }));

            mode == "create" ?
                    $(".list-holder", this.$el).append(item_el) :
                    $("#" + model.get("id"), this.$el).replaceWith($(item_el));

        },
        searchByName: function(e) {
            var search_text = $(e.target).val();

            if (e.type == 'keyup' && e.keyCode != 13)
                return;

            if (search_text == "") {
                $(".input-group-addon i", this.$el).hide();
                return;
            }

            $(".input-group-addon i", this.$el).show();


            this.reset();


            this.fetch({
                name: search_text
            });

        },
        resetSerahcByName: function() {
            $(".input-group-addon i", this.$el).hide();
            $(".search", this.$el).val("");

            this.collection.filters.unset("name", {
                silent: true
            });
            var filters = this.collection.filters.toJSON();

            this.reset();
            this.fetch(filters);
        },
        loadMoreItems: function() {

            if (!this.hasMoreItems)
                return;

            var $this = this;

            this.collection.nextPage().fetch({
                add: true,
                update: true,
                remove: false,
                success: function(collection, response) {
                    if (response.count <= 0) {
                        $this.hasMoreItems = false;
                    }
                }
            });
        }


    });
    return BaseListView
});