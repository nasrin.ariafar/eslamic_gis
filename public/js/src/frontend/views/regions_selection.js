define([
    'jquery',
    'backbone',
    'underscore',
    'frontend/views/base',
    'text!frontend/templates/regions_selection.html',
    'i18n!nls/labels',
    'icheck',
 
    ], function($, Backbone, _, BaseView, Tpl, Labels) {
        window._views = [];


        var RegionsSelectionView = BaseView.extend({

            className : "regions-selection",

            initialize : function(){
                this.render();
                return this;
            },

            render : function(){
                var tpl = _.template(Tpl);
                this.$el.html(tpl({
                    regions           : this.collection.toJSON(),
                    selected_regions  : [],
                    labels            : Labels,
                    checked           : this.options.checked || false
                }));
            },

            registerEvents : function(){

                var $this = this;
                
                $('input', this.$el).iCheck({
                    checkboxClass: 'icheckbox_flat-purple',
                    radioClass: 'iradio_flat-purple'
                });


                $('input.region-checkbox', this.$el).on('ifClicked', function(){
                    $this.onClickRegion($(this));
                });

                $('input.area-checkbox', this.$el).on('ifClicked', function(){
                    $this.onClickArea($(this));
                });

                $('input.country-checkbox', this.$el).on('ifToggled', function(){
                    $this.onToggleCountry($(this));
                });

                $('input.area-checkbox', this.$el).on('ifToggled', function(){
                    $this.onToggleArea($(this));
                });

                $("input", this.$el).css("visibility", "visible");

            },

            onClickRegion : function(el){

                var region_id = el.data("region"),
                holder = $("#collapse-region-" + region_id);

                if(!el.is(":checked"))
                    $("input", holder).iCheck('check');
                else
                    $("input", holder).iCheck('uncheck');
            },

            onClickArea : function(el){
                var area_id = el.data("area"),
                holder = $("#collapse-area-" + area_id);

                if(!el.is(":checked")){
                    $("input", holder).iCheck('check');
                }
                else{
                    $("input", holder).is(":checked") && $("input", holder).iCheck('uncheck');
                }
            },

            onToggleArea : function(el){
                var parent = $(el.closest(".panel-group")),
                region_id = el.data("region");

                if( $(".area-checkbox:checked", parent).length == $(".area-checkbox", parent).length ){
                    $("input#region-" + region_id).iCheck('check');
                }else{
                    $("input#region-" + region_id).is(":checked") && $("input#region-" + region_id).iCheck('uncheck');
                }
            },

            onToggleCountry : function(el){
                var area_id = el.data("area"),
                parent = $(el.closest(".panel-body"));

                if(el.is(":checked")){
                    if( $(".country-checkbox:checked", parent).length == $(".country-checkbox", parent).length )
                        $("input#area-" + area_id).iCheck('check');
                }else{
                    $("input#area-" + area_id).is(":checked") && $("input#area-" + area_id).iCheck('uncheck');
                }

                this.options.onSelectCountry && this.options.onSelectCountry(el);
            }


        });

        return RegionsSelectionView;
    });
