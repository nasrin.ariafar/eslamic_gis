define([
    'jquery',
    'backbone',
    'underscore',
    'frontend/views/base',
    'frontend/models/place',
    'text!frontend/templates/place_details.html',
    'i18n!nls/labels',

    ], function($, Backbone, _, BaseView, PlaceModel, Tpl, Labels) {
        
        var PlaceDetailsView = BaseView.extend({

            className : "place-details-page",

            initialize : function(options){
                
                this.options = options;
                this.model = new PlaceModel({
                    id : this.options.place_id
                });
                this.getPlace();
            },

            getPlace : function(){
                var $this = this;
                
                this.model.fetch({
                    success : function(){
                        $this.render();
                    },
                    error : function(error){
                        
                    }
                })
            },

            render : function(){

                var tpl = _.template(Tpl);
                this.$el.html(tpl(_.extend(this.model.toJSON(),{
                    labels : Labels
                })));
            }



        });

        return PlaceDetailsView;
    });
