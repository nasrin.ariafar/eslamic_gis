define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'frontend/views/base',
    'text!frontend/templates/layout.html',
    'frontend/views/map_view',
    'i18n!nls/labels',
    'frontend/views/change_password',
    'frontend/views/regions',
    'frontend/views/place_types',
    'frontend/views/users',
    'frontend/views/user_form',
    'frontend/views/place_form',
    'frontend/views/places',
    'icheck',
  
    ], function ($, Backbone, _, App, BaseView, LayoutTpl , MapView, Labels, ChangepasswordView) {

        var loginView = BaseView.extend({

            el : $("#wrapper"),
            
            events : {
                'keydown input'             : 'preventDefualt',
                'click #side-menu li'       : 'select',
                'click .switch-list'        : 'switchList',
                'click .logout'             : 'logout',
                'click .witch-map-view'     : 'siwtchMapView',
                'click .close-map-view'     : 'exitMapView',
                'click .change-password'    : 'popChangepasswordForm'
            },

            initialize: function(options) {
                this.options = options;
                this.render();
            },

            render : function() {
                this.template = _.template(LayoutTpl);
                
                this.$el.html(this.template({
                    labels : Labels
                }));
                this.afterRender();
                return this;
            },

            afterRender : function(){
                $(".footer-nav a").tooltip({
                    trigger : "hover"
                });

                if(window.currentUser.get("permission") == 3)
                    this.siwtchMapView();
            },

            select : function(e){
                var el = $(e.target).closest("li");
                if(el.hasClass("active"))
                    return;

                else{
                    $("#side-menu .active").removeClass("active");
                    el.addClass("active");
                }
            },

            switchList : function(e){
                var list_type = $(e.target).closest("li").data("type");
                var list_view = require('frontend/views/' + list_type);

                this.subView = new list_view();
                $("#page-wrapper").html(this.subView.$el);
                this.subView.afterRender && this.subView.afterRender();
            },

            logout : function(){
                $.ajax({
                    url     : '/users/auth/logout',
                    type: "POST",
                    success : function(resp){
                        window.currentUser.clear();
                        window.location = '';
                    },
                    error : function(error){
                        console.log(error)
                    }
                }, this);
            },

            siwtchMapView : function(){
                $("#visitor-menu").height("calc(100% - 60px)");
                $("#page-wrapper").css("background-color", "#E5E3DF")
                $("#visitor-menu", this.$el).animate({
                    width: $(window).width() * 0.25 + "px"
                }, {
                    duration: 200,
                    done: function() {
                        //                        $("#default-menu").hide();
                        $("#visitor-menu", this.$el).css("overflow", "visible")
                    }
                });

                this.showMapView();
            },

            exitMapView : function(){
                $("#visitor-menu").height("۰");
                $("#page-wrapper").css("background-color", "#FFFFFF")
                $("#visitor-menu", this.$el).animate({
                    width: 0
                }, {
                    duration: 200,
                    done: function() {
                        //                        $("#default-menu").hide();
                        $("#visitor-menu", this.$el).css("overflow", "hidden")
                    }
                });

                $("#page-wrapper").empty();
            },

            showMapView : function(){
                this.mapView = new MapView({
                    el : this.$el
                });

                this.mapView.initMap();
            },

            popChangepasswordForm : function(){
                this.changePasswordView && this.changePasswordView.dispose();

                this.changePasswordView = new ChangepasswordView();
                $(".change-password-modal", this.$el).html(this.changePasswordView.$el);
                $(".change-password-modal", this.$el).modal("show");
            }
        });

        return loginView;
    });
