define([
    'jquery',
    'jquery-ui',
    'underscore',
    'backbone',
    'frontend/views/login',
    'frontend/views/layout',
    'frontend/views/place_details',
    ], function($, jqueryUi,  _, Backbone, LoginView, LayoutView, PlaceDetails ) {

        var AppRouter = Backbone.Router.extend({
            routes: {
                ''                  : 'home',
                'places/:id'        : 'placePage'
            },

            home : function(){
                if(!window.currentUser.get("id")){
                    var loginView = new LoginView();
                    $("#wrapper").html(loginView.render().$el);
                }
                else{
                    this.layoutView = new LayoutView();
                }
            },

            placePage : function(id){
                this.view = new PlaceDetails({
                    place_id    : id
                });
                $("#wrapper").html(this.view.$el);
            }

        });

        return AppRouter;
    });
