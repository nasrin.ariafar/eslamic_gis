({
//    appDir: './',
    baseUrl: "./js/src",
    mainConfigFile: './js/src/main.js',
    dir: './js/release',
    logLevel: 0,
    preserveLicenseComments: !1,
    optimize: "none",
    removeCombined: true,
    optimizeCss: 'standard',
    findNestedDependencies: true,
    skipDirOptimize: true,
    keepBuildDir: false,
    wrapShim: true,
    // 3rd party script alias names
    paths: {
        'jquery': 'vendor/jquery/jquery',
        'jquery-ui': 'vendor/jquery-ui/js/jquery-ui-1.10.4.custom',
        'underscore': 'vendor/underscore/underscore',
        'underscore.string': 'vendor/underscore/underscore.string',
        'backbone': 'vendor/backbone/backbone',
        'bootstrap': 'vendor/bootstrap/js/bootstrap.min',
        'text': 'vendor/requirejs/requirejs-text',
        'json': 'vendor/requirejs/json',
        'i18n': 'vendor/requirejs/i18n',
        "select_picker": 'vendor/bootstrap-select/js/bootstrap-select',
        'icheck': 'vendor/iCheck-1.x/icheck.min',
        'pnotify': 'vendor/pnotify/pnotify.custom.min',
        'bootbox': 'vendor/bootbox/bootbox',
    },
    // Sets the use.js configuration for your application
    use: {
        'bootstrap': {
            deps: ['jquery']
        },
        'underscore.string': {
            deps: ['underscore']
        },
        'select_picker': {
            deps: ['bootstrap']
        },
        'jquery-ui': {
            deps: ['jquery']
        },
        'icheck': {
            deps: ['jquery']
        },
        'bootbox': {
            deps: ['jquery', 'bootstrap']
        }
    },
    name: "main"

})
