<?php

class Tea_FileUploader {

    private $uploadPath = '';
    private $fileName = '';
    private $currByte = '';
    private $maxFileSize = '';
    private $html5fsize = '';
    private $isLast = '';
    private $thumbHeight = '';
    private $thumbWidth = '';
    private $thumbPostfix = '';
    private $thumbPath = '';
    private $thumbFormat = '';
    private $allowExt = '';
    private $fullPath = '';

    function __construct() {
        $this->uploadPath = $_REQUEST['ax-file-path'];
        
        if (isset($_REQUEST['ax-file-name'])){ //comming from resources
            $this->fileName = $_REQUEST['ax-file-name'];
        } else { // comming from albums
            $this->fileName = $_REQUEST['name'];
        }
        
        $this->currByte = $_REQUEST['ax-start-byte'];
        $this->maxFileSize = $_REQUEST['ax-maxFileSize'];
        $this->html5fsize = $_REQUEST['ax-fileSize'];
        $this->isLast = $_REQUEST['isLast'];

        //if set generates thumbs only on images type files
        $this->thumbHeight = $_REQUEST['ax-thumbHeight'];
        $this->thumbWidth = $_REQUEST['ax-thumbWidth'];
        $this->thumbPostfix = $_REQUEST['ax-thumbPostfix'];
        $this->thumbPath = $_REQUEST['ax-thumbPath'];
        $this->thumbFormat = $_REQUEST['ax-thumbFormat'];

        $this->allowExt = (empty($_REQUEST['ax-allow-ext'])) ? array() : explode('|', $_REQUEST['ax-allow-ext']);
        $this->uploadPath .= ( !in_array(substr($this->uploadPath, -1), array('\\', '/')) ) ? '/' : ''; //normalize path

        if (!file_exists($this->uploadPath) && !empty($this->uploadPath)) {
            mkdir($this->uploadPath, 0777, true);
        }

        if (!file_exists($this->thumbPath) && !empty($this->thumbPath)) {
            mkdir($this->thumbPath, 0777, true);
        }


        if (isset($_FILES['ax-files'])) {
            //for eahc theorically runs only 1 time, since i upload i file per time
            foreach ($_FILES['ax-files']['error'] as $key => $error) {
                if ($error == UPLOAD_ERR_OK) {
                    $newName = !empty($this->fileName) ? $this->fileName : $_FILES['ax-files']['name'][$key];
                    $this->fullPath = $this->checkFilename($newName, $_FILES['ax-files']['size'][$key]);

                    if ($this->fullPath) {
                        move_uploaded_file($_FILES['ax-files']['tmp_name'][$key], $this->fullPath);
                        if (!empty($this->thumbWidth) || !empty($this->thumbHeight))
                            $this->createThumbGD($this->fullPath, $this->thumbPath, $this->thumbPostfix, $this->thumbWidth, $this->thumbHeight, $this->thumbFormat);

                        return array('name' => basename($this->fullPath), 'size' => filesize($this->fullPath), 'status' => 'uploaded', 'info' => 'File uploaded');
                    }
                }
                else {
                    return array('name' => basename($_FILES['ax-files']['name'][$key]), 'size' => $_FILES['ax-files']['size'][$key], 'status' => 'error', 'info' => $error);
                }
            }
        } elseif (isset($_REQUEST['ax-file-name'])) {
            //check only the first peice
            $this->fullPath = ($this->currByte != 0) ? $this->uploadPath . $this->fileName : $this->checkFilename($this->fileName, $this->html5fsize);

            if ($this->fullPath) {
                $flag = ($this->currByte == 0) ? 0 : FILE_APPEND;
                $receivedBytes = file_get_contents('php://input');
                //strange bug on very fast connections like localhost, some times cant write on file
                //TODO future version save parts on different files and then make join of parts
                    while (@file_put_contents($this->fullPath, $receivedBytes, $flag) === false) {
                    usleep(50);
                }

                if ($this->isLast == 'true') {
                    $this->createThumbGD($this->fullPath, $this->thumbPath, $this->thumbPostfix, $this->thumbWidth, $this->thumbHeight, $this->thumbFormat);
                }
                return array('name' => basename($this->fullPath), 'size' => $this->currByte, 'status' => 'uploaded', 'info' => 'File/chunk uploaded');
            }
        }
    }

    public function checkFilename($fileName, $size, $newName = '') {

        //------------------max file size check from js
        $maxsize_regex = preg_match("/^(?'size'[\\d]+)(?'rang'[a-z]{0,1})$/i", $this->maxFileSize, $match);
        $maxSize = 4 * 1024 * 1024; //default 4 M
        if ($maxsize_regex && is_numeric($match['size'])) {
            switch (strtoupper($match['rang'])) {//1024 or 1000??
                case 'K': $maxSize = $match[1] * 1024;
                    break;
                case 'M': $maxSize = $match[1] * 1024 * 1024;
                    break;
                case 'G': $maxSize = $match[1] * 1024 * 1024 * 1024;
                    break;
                case 'T': $maxSize = $match[1] * 1024 * 1024 * 1024 * 1024;
                    break;
                default: $maxSize = $match[1]; //default 4 M
            }
        }

        if (!empty($this->maxFileSize) && $size > $maxSize) {
            return array('name' => $fileName, 'size' => $size, 'status' => 'error', 'info' => 'File size not allowed.');
            return false;
        }
        //-----------------End max file size check
        //comment if not using windows web server
        $windowsReserved = array('CON', 'PRN', 'AUX', 'NUL', 'COM1', 'COM2', 'COM3', 'COM4', 'COM5', 'COM6', 'COM7', 'COM8', 'COM9',
            'LPT1', 'LPT2', 'LPT3', 'LPT4', 'LPT5', 'LPT6', 'LPT7', 'LPT8', 'LPT9');
        $badWinChars = array_merge(array_map('chr', range(0, 31)), array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));

        $fileName = str_replace($badWinChars, '', $fileName);
        $fileInfo = pathinfo($fileName);
        $fileExt = $fileInfo['extension'];
        $fileBase = $fileInfo['filename'];
        $this->fileType = $fileExt ;

        //check if legal windows file name
        if (in_array($fileName, $windowsReserved)) {
            return array('name' => $fileName, 'size' => 0, 'status' => 'error', 'info' => 'File name not allowed. Windows reserverd.');
            return false;
        }

        //check if is allowed extension
        if (!in_array($fileExt, $this->allowExt) && count($this->allowExt)) {
            return array('name' => $fileName, 'size' => 0, 'status' => 'error', 'info' => "Extension [$fileExt] not allowed.");
            return false;
        }

        $this->fullPath = $this->uploadPath . $fileName;
        $c = 0;
        while (file_exists($this->fullPath)) {
            $c++;
            $fileName = $fileBase . "($c)." . $fileExt;
            $this->fullPath = $this->uploadPath . $fileName;
        }
        return $this->fullPath;
    }

    public function createThumbGD($filepath, $thumbPath, $postfix, $maxwidth, $maxheight, $format = 'jpg', $quality = 75) {
        if ($maxwidth <= 0 && $maxheight <= 0) {
            return 'No valid width and height given';
        }

        $gd_formats = array('jpg', 'jpeg', 'png', 'gif'); //web formats
        $file_name = pathinfo($filepath);
        if (empty($format))
            $format = $file_name['extension'];

        if (!in_array(strtolower($file_name['extension']), $gd_formats)) {
            return false;
        }

        $thumb_name = $file_name['filename'] . $postfix . '.' . $format;

        if (empty($this->thumbPath)) {
            $this->thumbPath = $file_name['dirname'];
        }
        $this->thumbPath.= ( !in_array(substr($this->thumbPath, -1), array('\\', '/')) ) ? DIRECTORY_SEPARATOR : ''; //normalize path
        // Get new dimensions
        list($width_orig, $height_orig) = getimagesize($filepath);
        if ($width_orig > 0 && $height_orig > 0) {
            $ratioX = $maxwidth / $width_orig;
            $ratioY = $maxheight / $height_orig;
            $ratio = min($ratioX, $ratioY);
            $ratio = ($ratio == 0) ? max($ratioX, $ratioY) : $ratio;
            $newW = $width_orig * $ratio;
            $newH = $height_orig * $ratio;

            // Resample
            $thumb = imagecreatetruecolor($newW, $newH);
            $image = imagecreatefromstring(file_get_contents($filepath));

            imagecopyresampled($thumb, $image, 0, 0, 0, 0, $newW, $newH, $width_orig, $height_orig);

            // Output
            switch (strtolower($format)) {
                case 'png':
                    imagepng($thumb, $this->thumbPath . $thumb_name, 9);
                    break;

                case 'gif':
                    imagegif($thumb, $this->thumbPath . $thumb_name);
                    break;

                default:
                    imagejpeg($thumb, $this->thumbPath . $thumb_name, $quality);
                    ;
                    break;
            }
            imagedestroy($image);
            imagedestroy($thumb);
        } else {
            return false;
        }
    }
    
//    function createThumbIM($filepath, $this->thumbPath, $postfix, $maxwidth, $maxheight, $format) {
//    $file_name = pathinfo($filepath);
//    $thumb_name = $file_name['filename'] . $postfix . '.' . $format;
//
//    if (empty($this->thumbPath)) {
//        $this->thumbPath = $file_name['dirname'];
//    }
//    $this->thumbPath.= ( !in_array(substr($this->thumbPath, -1), array('\\', '/')) ) ? DIRECTORY_SEPARATOR : ''; //normalize path
//
//    $image = new Imagick($filepath);
//    $image->thumbnailImage($maxwidth, $maxheight);
//    $images->writeImages($this->thumbPath . $thumb_name);
//}
    
    public function getFileName()
    {
        return $this->fileName;
    }
    
    public function getFullPath()
    {
        return ($this->fullPath);
    }
    public function getType()
    {
        return ($this->fileType);
    }
}


?>
