<?php

class Tea_Controller_Action_Helper_ContextSwitch
    extends Zend_Controller_Action_Helper_ContextSwitch
{
    public function __construct($options = null)
    {
        $this->addContexts(
            array(
                'json' => array(
                    'suffix'    => 'json',
                    'headers'   => array('Content-Type' => 'application/json'),
                ),
                'ajax' => array(
                    'suffix'    => 'ajax',
                    'headers'   => array('Content-Type' => 'text/html'),
                )
            )
        );
        parent::__construct($options);
    }
}
