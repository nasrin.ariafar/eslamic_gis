<?php
//namespace Everyman\Neo4j\Command;
//use Everyman\Neo4j\Command,
//	Everyman\Neo4j\Client,
//	Everyman\Neo4j\Exception,
//	Everyman\Neo4j\Relationship;

/**
 * Update a relationship's properties
 */
class Tea_Neo4j_Command_UpdateRelationship extends Tea_Neo4j_Command
{
	protected $rel = null;

	/**
	 * Set the relationship to drive the command
	 *
	 * @param Tea_Neo4j_Client $client
	 * @param Tea_Neo4j_Relationship $rel
	 */
	public function __construct(Tea_Neo4j_Client $client, Tea_Neo4j_Relationship $rel)
	{
		parent::__construct($client);
		$this->rel = $rel;
	}

	/**
	 * Return the data to pass
	 *
	 * @return mixed
	 */
	protected function getData()
	{
		return $this->rel->getProperties();
	}

	/**
	 * Return the transport method to call
	 *
	 * @return string
	 */
	protected function getMethod()
	{
		return 'put';
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		if (!$this->rel->hasId()) {
			throw new Tea_Neo4j_Exception('No relationship id specified');
		}
		return '/relationship/'.$this->rel->getId().'/properties';
	}

	/**
	 * Use the results
	 *
	 * @param integer $code
	 * @param array   $headers
	 * @param array   $data
	 * @return boolean true on success
	 * @throws Tea_Neo4j_Exception on failure
	 */
	protected function handleResult($code, $headers, $data)
	{
		if ((int)($code / 100) == 2) {
			$this->getEntityCache()->setCachedEntity($this->rel);
			return true;
		} else {
			$this->throwException('Unable to update relationship', $code, $headers, $data);
		}
	}
}

