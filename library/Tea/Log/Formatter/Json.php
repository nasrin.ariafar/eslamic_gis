<?php

/** Zend_Log_Formatter_Abstract */
require_once 'Zend/Log/Formatter/Abstract.php';

class Tea_Log_Formatter_Json extends Zend_Log_Formatter_Abstract
{
    /**
     * @var string Name of root element
     */
    protected $_rootElement;

    /**
     * @var array Relates XML elements to log data field keys.
     */
    protected $_elementMap;

    /**
     * @var string Encoding to use in XML
     */
    protected $_encoding;

    /**
     * Class constructor
     * (the default encoding is UTF-8)
     *
     * @param array|Zend_Config $options
     * @return void
     */
    public function __construct($options = array())
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        } elseif (!is_array($options)) {
            $args = func_get_args();

            $options = array(
                'rootElement' => array_shift($args)
            );

            if (count($args)) {
                $options['elementMap'] = array_shift($args);
            }

            if (count($args)) {
                $options['encoding'] = array_shift($args);
            }
        }

        if (!array_key_exists('rootElement', $options)) {
            $options['rootElement'] = 'logEntry';
        }

        if (!array_key_exists('encoding', $options)) {
            $options['encoding'] = 'UTF-8';
        }

        $this->_rootElement = $options['rootElement'];

        if (array_key_exists('elementMap', $options)) {
            $this->_elementMap  = $options['elementMap'];
        }
    }

    /**
     * Factory for Zend_Log_Formatter_Xml classe
     *
     * @param array|Zend_Config $options
     * @return Zend_Log_Formatter_Xml
     */
    public static function factory($options)
    {
        return new self($options);
    }

    /**
     * Formats data into a single line to be written by the writer.
     *
     * @param  array    $event    event data
     * @return string             formatted line to write to the log
     */
    public function format($event)
    {
        if ($this->_elementMap === null) {
            $dataToInsert = $event;
        } else {
            $dataToInsert = array();
            foreach ($this->_elementMap as $elementName => $fieldKey) {
                $dataToInsert[$elementName] = $event[$fieldKey];
            }
        }

        $result = array();

        foreach ($dataToInsert as $key => $value) {
            if (empty($value)
                || is_scalar($value)
                || (is_object($value) && method_exists($value,'__toString'))
            ) {
                if($key == "message") {
                    $value = htmlspecialchars($value, ENT_COMPAT, 'utf-8');
                }
                $result[$key] = (string)$value;
            } else {
                $result[$key] = json_encode($value);
            }
        }

        return json_encode($result) . PHP_EOL;
    }
}
