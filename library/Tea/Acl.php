<?php

class Tea_Acl extends Zend_Acl
{
    public function isAccessible($context, $action, $item_id = '*')
    {
        $checkPerms = array(
            array(
                'context' => $context,
                'action'  => $action,
                'item_id' => $item_id
            ),

            array(
                'context' => $context,
                'action'  => $action,
                'item_id' => '*'
            ),
            array(
                'context' => $context,
                'action'  => '*',
                'item_id' => $item_id
            ),
            array(
                'context' => $context,
                'action'  => '*',
                'item_id' => '*'
            ),
            array(
                'context' => '*',
                'action'  => '*',
                'item_id' => '*'
            )
        );

        $perSrv = Users_Service_RolePermission::getInstance();
        $user   = Users_Service_Auth::getInstance()->getCurrentUser();
        if ($user instanceof Users_Model_User) {
            $uid = $user->getId();
        } else{
            $uid = 0;
        }

        foreach ($checkPerms as $perm) {
            $access = $perSrv->isAccessible(
                $uid,
                $perm['context'],
                $perm['action'],
                $perm['item_id']
            );

            if ($access) {
                return true;
            }
        }

        return false;
    }
}
