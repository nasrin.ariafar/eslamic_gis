<?php

/*
 * sample usage
 * Tea_Service_Gearman::getInstance()->doBackground('module', 'email', 'alireza.meskin@gmail.com');
 */
class Tea_Service_Gearman
{
    private static $_instance = null;

    private $gClient = null;

    private $prefix  = '';

    private function __construct()
    {
        $configs = Zend_Registry::get('config');
        if ($configs->gearman->enable) {
            $this->gClient = new GearmanClient();
            if (isset($configs->gearman) && isset($configs->gearman->servers)) {
                $this->gClient->addServers($configs->gearman->servers);
            } else {
                $this->gClient->addServer();
            }

            if (isset($configs->gearman->prefix_workers)) {
                $this->prefix = $configs->gearman->prefix_workers;
            }
        }
    }

    public static function getInstance()
    {
        if (self::$_instance == null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __call($method, $arguments)
    {
        $configs = Zend_Registry::get('config');
        if (!$configs->gearman->enable) {
            $modulePath  = strtolower($arguments[0]);
            $moduleName  = str_replace('_', ' ', strtolower($arguments[0]));
            $moduleName  = str_replace(' ', '', ucwords($moduleName));

            $workerName  = str_replace('_', ' ', strtolower($arguments[1]));
            $workerName  = str_replace(' ', '', ucwords($workerName));
            $workerFile  = APPLICATION_PATH . '/modules/'.$modulePath.'/workers/' . $workerName . '.php';
            $workerClass = $moduleName . '_Worker_' . $workerName;

            if (!file_exists($workerFile)) {
                throw new Exception('The worker file does not exist: ' . $workerFile);
            }
            include_once $workerFile;

            if (!class_exists($workerClass)) {
                throw new Exception(
                    'The worker class: ' . $workerClass.
                    ' does not exist in file: ' . $workerFile
                );
            }
            if (Zend_Registry::isRegistered('bootstrap')) {
                $bootstrap = Zend_Registry::get('bootstrap');
            } else {
                $front  = Zend_Controller_Front::getInstance();
                $bootstrap = $front->getParam("bootstrap");
            }
            $worker = new $workerClass($bootstrap);
            return $worker->work($arguments[2]);
        }

        switch ($method) {
        case 'do':
        case 'doBackground':
        case 'doHigh' :
        case 'doHighBackground' :
        case 'doLow':
        case 'doLowBackground':
        case 'doNormal':
            $module = strtolower($arguments[0]);
            $worker = strtolower($arguments[1]);
            $args   = array(
                $this->prefix . $module . '::' . $worker,
                serialize($arguments[2])
            );

            return call_user_func_array(array($this->gClient, $method), $args);
            break;

        default :
            throw new Exception('The method : ' . $method. ' does not exits');
            break;
        }
    }

    public function getGearmanClient()
    {
        return $this->gClient;
    }
}
