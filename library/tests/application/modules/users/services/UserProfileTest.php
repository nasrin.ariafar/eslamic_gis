<?php

class Users_Service_ProfileTest extends Tea_Test_ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('users');
        $this->cleanupTable('user_profiles');
    }

    public function tearDown()
    {
    }

    public function testGetInstance()
    {
        $instance = Users_Service_Profile::getInstance();
        $this->assertInstanceOf('Users_Service_Profile', $instance);
    }

    public function testCreateUserProfile()
    {
        /*
         * user is created
         * a profile for this user is created
         * 
         * user's profile retrieves by profile_id, 
         * and it's properties compare with original profile
         */
        
        $userService        = Users_Service_User::getInstance();
        $userProfileService = Users_Service_Profile::getInstance();
        
        $user = $this->createTestUser();
        
        $userProfile = new Users_Model_Profile();
        $userProfile->setCountry('Iran');
        $userProfile->setCreatedById($user->getId());
        $userProfile->setUpdatedById($user->getId());
        $userProfile = $userProfileService->save($userProfile);
        
        $user->setProfileId($userProfile->getId());
        
        $testProfile = $userProfileService->getByPK($userProfile->getId());
        $this->assertEquals($testProfile->getCountry(), 'Iran');
        $this->assertEquals($testProfile->getDeleted(), 0);
    }    
    
    public function testUpdateUserProfile()
    {
        /*
         * user is created
         * a profile for this user is created
         * country of this profile, is updated
         * 
         * user's profile retrieves by profile_id, 
         * and it's properties compare with modified profile
         */
        
        $userService        = Users_Service_User::getInstance();
        $userProfileService = Users_Service_Profile::getInstance();
        
        $user           = $this->createTestUser();
        $userProfile    = $this->createTestUserProfile($user);
        
        $userProfile = $userProfileService->getByPK($userProfile->getId());
        $userProfile->setCountry('');
        $userProfile->setUpdatedById($user->getId());
        $userProfile->setUpdateDate(Tea_Model_Entity::getDateTime('now'));
        $userProfile->setNew(false);
        $userProfile = $userProfileService->save($userProfile);
        
        $testProfile = $userProfileService->getByPK($userProfile->getId());
        $this->assertEquals($testProfile->getCountry(), '');
        $this->assertEquals($testProfile->getDeleted(), 0);
    }

    public function testDeleteUserProfile()
    {
        /*
         * user is created
         * a profile for this user is created
         * user's profile is deleted
         * 
         * user's profile retrieves by profile_id, 
         * and result of this search should be null
         */

        $userProfileService = Users_Service_Profile::getInstance();
        
        $user           = $this->createTestUser();
        $userProfile    = $this->createTestUserProfile($user);
        
        $userProfileService->remove($userProfile);
        $testProfile = $userProfileService->getByPK($userProfile->getId());
        $this->assertEquals($testProfile, null);
    }
}
