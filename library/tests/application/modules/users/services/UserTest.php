<?php

class Users_Service_UserTest extends Tea_Test_ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('users');
    }

    public function tearDown()
    {
    }

    public function testGetInstance()
    {
        $instance = Users_Service_User::getInstance();
        $this->assertInstanceOf('Users_Service_User', $instance);
    }

    public function testCreateUser()
    {
        /*
         * user is created
         * 
         * user retrieves by id, 
         * and it's properties compare with original user
         */
        
        $userService = Users_Service_User::getInstance();
        
        $user = new Users_Model_User();
        $user->setCorpId('');
        $user->setDeptId('');
        $user->setFirstName('first_name');
        $user->setLastName('last_name');
        $user->setEmail(TEST_USER_EMAIL);
        $user->setUsername('test');
        $user->setPassword(TEST_USER_PASS);
        $user->setGender(Users_Model_User::GENDER_MALE);
        $user->setTimezone('timezone');
        $user->setLocale('locale');
        $user->setCreatedById('');
        $user->setUpdatedById('');
        $user = $userService->save($user);
        
        $testUser = $userService->getByPK($user->getId());
        $this->assertEquals($testUser->getFirstName(), 'first_name');
        $this->assertEquals($testUser->getLastName(), 'last_name');
        $this->assertEquals($testUser->getEmail(), TEST_USER_EMAIL);
        $this->assertEquals($testUser->getUsername(), 'test');
        $this->assertEquals($testUser->getPassword(), TEST_USER_PASS);
        $this->assertEquals($testUser->getGender(), Users_Model_User::GENDER_MALE);
        $this->assertEquals($testUser->getDeleted(), 0);
    }    
    
    public function testUpdateUser()
    {
        /*
         * user is created
         * first_name and middle_name of this user, is updated
         * 
         * user retrieves by id, 
         * and it's properties compare with modified user
         */
        
        $userService = Users_Service_User::getInstance();
        
        $user = $this->createTestUser();
        
        $user = $userService->getByPK($user->getId());
        $user->setFirstName('modified first_name');
        $user->setMiddleName('modified middle_name');
        $user->setUpdatedById($user->getId());
        $user->setUpdateDate(Tea_Model_Entity::getDateTime('now'));
        $user->setNew(false);
        $user = $userService->save($user);
        
        $testUser = $userService->getByPK($user->getId());
        $this->assertEquals($testUser->getFirstName(), 'modified first_name');
        $this->assertEquals($testUser->getMiddleName(), 'modified middle_name');
        $this->assertEquals($testUser->getLastName(), 'last_name');
        $this->assertEquals($testUser->getDeleted(), 0);
    }

    public function testDeleteUser()
    {
        /*
         * user is created
         * user is deleted
         * 
         * user retrieves by id, 
         * and result of this search should be null
         */

        $userService = Users_Service_User::getInstance();
        
        $user = $this->createTestUser();
        
        $userService->remove($user);
        $testUser = $userService->getByPK($user->getId());
        $this->assertEquals($testUser, null);
    }
}
