<?php

class Sites_Service_FollowTest extends Tea_Test_ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testGetInstance()
    {
        $instance = Sites_Service_Follow::getInstance();
        $this->assertInstanceOf('Sites_Service_Follow', $instance);
    }

    public function testCreateFollow()
    {
        $table = new Sites_Model_DbTable_Follows();
        $table->delete('');

        $now = date('Y-m-d H:i:s');

        $f = new Sites_Model_Follow();
        $f->setUserId(1);
        $f->setSiteId(2);
        $f->setStatus(Sites_Model_Follow::STATUS_ACCEPT);
        $f->setCreationDate($now);
        $f->setUpdateDate($now);

        Sites_Service_Follow::getInstance()->save($f);

        $this->assertInstanceOf('Sites_Model_Follow', $f);

        $f = Sites_Service_Follow::getInstance()->getByPK(1, 2);
        $this->assertInstanceOf('Sites_Model_Follow', $f);
        $this->assertEquals($f->getUserId(), 1);
        $this->assertEquals($f->getSiteId(), 2);
        $this->assertEquals($f->getCreationDate(), $now);
        $this->assertEquals($f->getUpdateDate(), $now);
        $this->assertEquals($f->getStatus(), Sites_Model_Follow::STATUS_ACCEPT);

        return array(
            $f->getUserId(),
            $f->getSiteId()
        );
    }

    /**
     * @depends testCreateFollow
     */
    public function testUpdate($id)
    {
        list($u, $s) = $id;

        $f = Sites_Service_Follow::getInstance()->getByPK($u, $s);
        $this->assertInstanceOf('Sites_Model_Follow', $f);

        $f->setStatus(Sites_Model_Follow::STATUS_PENDING);
        Sites_Service_Follow::getInstance()->save($f);

        $f = Sites_Service_Follow::getInstance()->getByPK(1, 2);
        $this->assertEquals($f->getStatus(), Sites_Model_Follow::STATUS_PENDING);
    }

    /**
     * depends testUpdate
     */
    public function testList()
    {
        $l = Sites_Service_Follow::getInstance()->getList(null, null, 0, $c, 10);
        $this->assertEquals($c, 1);
        $f = array_pop($l);
        $this->assertInstanceOf('Sites_Model_Follow', $f);
    }
}
