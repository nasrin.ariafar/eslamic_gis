<?php

class Sites_Api_MembersControllerTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testIndexAction()
    {
        $now=  Tea_Model_Entity::getDateTime('now');
        $siteService = Sites_Service_Site::getInstance();
        $site        = new Sites_Model_Site;
        $site->setTitle('testing');
        $site->setStatus('NOTPUBLISHED');
        $site->setDescription('testing');
        $site->setOwnerId(1);
        $site->setUpdatedById(1);
        $site->setStyleId(1);
        $site->setCreatedById(1);
        $site->setUpdateDate($now);
        $site->setCreationDate($now);

        $site        = $siteService->save($site);
        $this->dispatch('/api/sites/' . $site->getId() . '/members');
        $this->assertResponseCode(200);
        $this->assertModule('sites');
        $this->assertController('Api_members');
        $this->assertAction('index');
        return $site->getId();
    }

    /**
     * @depends testIndexAction
     */
    public function testPostAction($id)
    {
        Sites_Service_Member::getInstance()->removeAll();

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.dev',
                    'password' => 'test'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'user_id'        => Users_Service_Auth::getInstance()->getCurrentUser()->getId(),
                    'site_id'        => $id,
                    'type'           => 'user',
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/sites/' . $id . '/members');
        $this->assertResponseCode(200);
        $result          = $this->response->getBody();
        $membershipArray = json_decode($result, true);
        $this->assertEquals($membershipArray['user_id'], Users_Service_Auth::getInstance()->getCurrentUser()->getId());
        $this->assertEquals($membershipArray['site_id'], $id);
        $this->assertEquals($membershipArray['type'], 'user');

        $this->resetRequest();
        $this->resetResponse();
        $this->dispatch('/api/sites/' . $id . '/members');
        $this->assertResponseCode(200);
        $this->assertModule('sites');
        $this->assertController('Api_members');
        $this->assertAction('index');
        $body   = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($this->response->getBody(), true);
        $this->assertEquals($result['count'], 1);
        return $id;
    }

    /**
     * @depends testIndexAction
     */
    public function testGetAction($id)
    {
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.dev',
                    'password' => 'test'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('Get');
        $this->dispatch('/api/sites/' . $id . '/members/' . Users_Service_Auth::getInstance()->getCurrentUser()->getId());
        $this->assertResponseCode(200);
        $this->assertModule('sites');
        $this->assertController('Api_members');
        $this->assertAction('get');
        $body   = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($body, true);
        $this->assertEquals($result['site_id'], $id);
        return $id;
    }

    /**
     * @depends testGetAction
     */
    public function testPutAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.dev',
                    'password' => 'test'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $this->request->setMethod('PUT')
            ->setPost(
                array(
                    'type'      => 'poweruser',
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/sites/' . $id . '/members/' . Users_Service_Auth::getInstance()->getCurrentUser()->getId());
        $this->assertResponseCode(200);
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('GET');
        $this->dispatch('/api/sites/' . $id . '/members/' . Users_Service_Auth::getInstance()->getCurrentUser()->getId());
        $this->assertResponseCode(200);
        $this->assertModule('sites');
        $this->assertController('Api_members');
        $this->assertAction('get');
        $body       = $this->response->getBody();
        $this->assertNotEmpty($body);
        $membership = json_decode($body, true);
        $this->assertEquals($membership['type'], 'poweruser');
        return $id;
    }

    /**
     * @depends testPutAction
     */
    public function testDeleteAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.dev',
                    'password' => TEST_USER_PASS
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);
        
         $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('DELETE');
        $this->dispatch('/api/sites/' . $id.'/members/'. Users_Service_Auth::getInstance()->getCurrentUser()->getId());
        $this->assertResponseCode(200);
        $this->assertModule('sites');
        $this->assertController('Api_members');
        $this->assertAction('delete');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'Removed');

        
    }

}

?>
