<?php

class Posts_Api_FollowsControllerTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testIndexAction()
    {
        $this->cleanupTable('follows');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');

        $user = $this->createTestUser();
        $post = $this->createTestPost($user);

        $this->dispatch('/api/posts/' . $post->getId() . '/follows');
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api_follows');
        $this->assertAction('index');
        return $post->getId();
    }

    /**
     * @depends testIndexAction
     */
    public function testPostAction($postId)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.com',
                    'password' => 'test'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);



        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'post_id'     => $postId,
                    'follower_id' => Users_Service_Auth::getInstance()->getCurrentUser()->getId(),
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/posts/' . $postId . '/follows');
        $this->assertResponseCode(200);
        $result       = $this->response->getBody();
        $postArray    = json_decode($result, true);
        $this->assertEquals($postArray['post_id'], $postId);
        $this->assertEquals($postArray['follower_id'], Users_Service_Auth::getInstance()->getCurrentUser()->getId());

        $this->resetRequest();
        $this->resetResponse();
        $this->dispatch('api/posts/' . $postId . '/follows');
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api_follows');
        $this->assertAction('index');
        $body   = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($this->response->getBody(), true);
        $this->assertEquals($result['count'], 1);

        return $postArray['post_id'];
    }

    /**
     * @depends testPostAction
     */
    public function testDeleteAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => TEST_USER_EMAIL,
                    'password' => TEST_USER_PASS
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);


        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('DELETE');
        $table  = new Posts_Model_DbTable_Follows();
        $follow = $table->select()->where('post_id =?', $id)->where('follower_id =?', Users_Service_Auth::getInstance()->getCurrentUser()->getId());
        $follow = $table->fetchRow($follow);
        $this->dispatch('/api/posts/' . $follow['post_id'] . '/follows/');
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api_follows');
        $this->assertAction('delete');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'unfollowed');
    }

}

?>
