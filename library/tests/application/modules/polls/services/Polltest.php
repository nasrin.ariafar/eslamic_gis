<?php

class Polls_Service_PollTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('follows');
        $this->cleanupTable('shares');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('poll_votes');
        $this->cleanupTable('poll_voting_items');
        $this->cleanupTable('poll_assignees');
        $this->cleanupTable('poll_voting_responses');
        $this->cleanupTable('polls');
        $this->cleanupTable('users');
    }

    public function testGetInstance()
    {
        $instance = Polls_Service_Poll::getInstance();
        $this->assertInstanceOf('Polls_Service_Poll', $instance);
    }

    public function testCreatePoll()
    {
        $pollService = Polls_Service_Poll::getInstance();
        $poll        = new Polls_Model_Poll();
        $user        = $this->createTestUser();
        $poll->setTitle('test title');
        $poll->setDescription('test description');
        $poll->setStatus(2);
        $poll->setPrivacy(1);
        $poll->setNew(true);
        $poll->setCreatedById($user->getId());
        $poll->setUpdatedById($user->getId());
        $poll        = $pollService->save($poll);
        $testpoll    = $pollService->getByPK($poll->getId());
        $this->assertEquals($poll, $testpoll);
    }

    public function testUpdatePoll()
    {
        $pollService = Polls_Service_Poll::getInstance();
        $poll        = new Polls_Model_Poll();
        $user        = $this->createTestUser();
        $poll->setTitle('test title');
        $poll->setDescription('test description');
        $poll->setStatus(2);
        $poll->setPrivacy(1);
        $poll->setNew(true);
        $poll->setCreatedById($user->getId());
        $poll->setUpdatedById($user->getId());
        $poll        = $pollService->save($poll);
        $poll->setNew(FALSE);
        $poll->setPrivacy(2);
        $poll->setStatus(1);
        $poll->setTitle('test title modified');
        $poll->setDescription('test body modified');
        $poll        = $pollService->save($poll);
        $this->assertEquals($poll->getPrivacy(), 2);
        $this->assertEquals($poll->getStatus(), 1);
        $this->assertEquals($poll->getTitle(), 'test title modified');
        $this->assertEquals($poll->getDescription(), 'test body modified');
    }

    public function testRemovePoll()
    {
        $pollService = Polls_Service_Poll::getInstance();
        $poll        = new Polls_Model_Poll();
        $user        = $this->createTestUser();
        $poll->setTitle('test title');
        $poll->setDescription('test description');
        $poll->setStatus(2);
        $poll->setPrivacy(1);
        $poll->setNew(true);
        $poll->setCreatedById($user->getId());
        $poll->setUpdatedById($user->getId());
        $poll        = $pollService->save($poll);
        $pollId      = $poll->getId();
        $pollService->remove($poll);
        $poll        = $pollService->getByPK($pollId);
        $this->assertEquals($poll, null);
    }

}

?>
