<?php

class Reminders_Service_ReminderTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('reminders');
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('follows');
        $this->cleanupTable('shares');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');
    }

    public function testGetInstance()
    {
        $instance = Reminders_Service_Reminder::getInstance();
        $this->assertInstanceOf('Reminders_Service_Reminder', $instance);
    }

    public function testCreateReminder()
    {
        $reminderService = Reminders_Service_Reminder::getInstance();
        $reminder        = new Reminders_Model_Reminder();
        $user            = $this->createTestUser();
        $model           = new Tea_Model_Entity();
        $now             = $model->getDateTime('now');

        $reminder->setUserId($user->getId());
        $reminder->setTitle('test');
        $reminder->setDescription('testing');
        $reminder->setType(1);
        $reminder->setStartDate(0);
        $reminder->setStatus(1);
        $reminder->setRefType(1);
        $reminder->setRefId(1);
        $reminder->setDeleted(0);
        $reminder->setCreatedById($user->getId());
        $reminder->setCreationDate($now);
        $reminder->setUpdatedById($user->getId());
        $reminder->setUpdateDate($now);



        $reminder->setNew(true);
        $reminder     = $reminderService->save($reminder);
        $testReminder = $reminderService->getByPK($reminder->getId());
        $this->assertEquals($reminder, $testReminder);
    }

    public function testUpdateReminder()
    {
        $reminderService = Reminders_Service_Reminder::getInstance();
        $reminder        = new Reminders_Model_Reminder();
        $user            = $this->createTestUser();
        $model           = new Tea_Model_Entity();
        $now             = $model->getDateTime('now');

        $reminder->setUserId($user->getId());
        $reminder->setTitle('test');
        $reminder->setDescription('testing');
        $reminder->setType(1);
        $reminder->setStartDate(0);
        $reminder->setStatus(1);
        $reminder->setRefType(1);
        $reminder->setRefId(1);
        $reminder->setDeleted(0);
        $reminder->setCreatedById($user->getId());
        $reminder->setCreationDate($now);
        $reminder->setUpdatedById($user->getId());
        $reminder->setUpdateDate($now);

        $reminder->setNew(true);
        $reminder     = $reminderService->save($reminder);
        $testReminder = $reminderService->getByPK($reminder->getId());

        $reminder->setNew(false);
        $reminder->setTitle('test modified');
        $reminder = $reminderService->save($reminder);
        $this->assertEquals($reminder->getTitle(), 'test modified');
    }

    public function testDeleteReminder()
    {
        $reminderService = Reminders_Service_Reminder::getInstance();
        $reminder        = new Reminders_Model_Reminder();
        $user            = $this->createTestUser();
        $model           = new Tea_Model_Entity();
        $now             = $model->getDateTime('now');

        $reminder->setUserId($user->getId());
        $reminder->setTitle('test');
        $reminder->setDescription('testing');
        $reminder->setType(1);
        $reminder->setStartDate(0);
        $reminder->setStatus(1);
        $reminder->setRefType(1);
        $reminder->setRefId(1);
        $reminder->setDeleted(0);
        $reminder->setCreatedById($user->getId());
        $reminder->setCreationDate($now);
        $reminder->setUpdatedById($user->getId());
        $reminder->setUpdateDate($now);

        $reminder->setNew(true);
        $reminder   = $reminderService->save($reminder);
        $reminderId = $reminder->getId();
        $reminderService->remove($reminder);
        $reminder   = $reminderService->getByPK($reminderId);
        $this->assertEquals($reminder, null);
    }

}

?>
