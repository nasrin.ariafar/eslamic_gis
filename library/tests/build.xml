<?xml version="1.0" encoding="UTF-8" ?>

<project name="Saffron" default="build">

    <property name="source" value="${basedir}/.." />
    <property name="appsource" value="${source}/application" />
    <property name="libsource" value="${source}/library" />

    <target name="build" depends="prepare,lint,phploc,pdepend,phpmd-ci,phpcs-ci,phpcpd,phpdox,phpunit,phpcb"/>

    <target name="build-parallel" depends="prepare,lint,tools-parallel,phpunit,phpcb"/>

    <target name="tools-parallel" description="Run tools in parallel">
        <parallel threadCount="2">
            <sequential>
                <antcall target="pdepend"/>
                <antcall target="phpmd-ci"/>
            </sequential>
            <antcall target="phpcpd"/>
            <antcall target="phpcs-ci"/>
            <antcall target="phploc"/>
            <antcall target="phpdox"/>
        </parallel>
    </target>

    <target name="clean" description="Cleanup build artifacts">
        <delete dir="${basedir}/build/api"/>
        <delete dir="${basedir}/build/code-browser"/>
        <delete dir="${basedir}/build/coverage"/>
        <delete dir="${basedir}/build/logs"/>
        <delete dir="${basedir}/build/pdepend"/>
        <delete dir="${basedir}/build/phpdox"/>
    </target>

    <target name="prepare" depends="clean" description="Prepare for build">
        <mkdir dir="${basedir}/build/api"/>
        <mkdir dir="${basedir}/build/code-browser"/>
        <mkdir dir="${basedir}/build/coverage"/>
        <mkdir dir="${basedir}/build/logs"/>
        <mkdir dir="${basedir}/build/pdepend"/>
        <mkdir dir="${basedir}/build/phpdox"/>
    </target>


    <target name="lint">
        <apply executable="php" failonerror="true">
            <arg value="-l" />

            <fileset dir="${source}">
                <include name="**/*.php" />
                <modified />
            </fileset>

            <fileset dir="${source}/tests">
                <include name="**/*.php" />
                <modified />
            </fileset>
        </apply>
    </target>

    <target name="phploc" description="Measure project size using PHPLOC">
        <exec executable="phploc">
            <arg value="--log-csv" />
            <arg value="${basedir}/build/logs/phploc.csv" />
            <arg value="--log-xml" />
            <arg value="${basedir}/build/logs/phploc.xml" />
            <arg path="${appsource}" />
        </exec>
    </target>

    <target name="pdepend" description="Calculate software metrics using PHP_Depend">
        <exec executable="pdepend">
            <arg value="--jdepend-xml=${basedir}/build/logs/jdepend.xml" />
            <arg value="--jdepend-chart=${basedir}/build/pdepend/dependencies.svg" />
            <arg value="--overview-pyramid=${basedir}/build/pdepend/overview-pyramid.svg" />
            <arg path="${appsource}" />
        </exec>
    </target>

    <target name="phpmd"
        description="Perform project mess detection using PHPMD">
        <exec executable="phpmd" failonerror="false">
            <arg line="${appsource} xml codesize,unusedcode
                --reportfile ${basedir}/build/logs/pmd.xml
                --suffixes php" />
        </exec>
    </target>

    <target name="phpmd-ci"
        description="Perform project mess detection using PHPMD">
        <exec executable="phpmd">
            <arg path="${appsource}" />
            <arg value="xml" />
            <arg value="${basedir}/build/phpmd.xml" />
            <arg value="--reportfile" />
            <arg value="${basedir}/build/logs/pmd.xml" />
        </exec>
    </target>

    <target name="phpcs"
        description="Find coding standard violations using PHP_CodeSniffer">
        <exec executable="phpcs">
            <arg value="--standard=PEAR" />
            <arg path="${appsource}" />
            <arg path="${libsource}/Tea" />
            <arg path="${basedir}/application" />
        </exec>
    </target>

    <target name="phpcs-ci"
        description="Find coding standard violations using PHP_CodeSniffer">
        <exec executable="phpcs" output="/dev/null">
            <arg value="--report=checkstyle" />
            <arg value="--report-file=${basedir}/build/logs/checkstyle.xml" />
            <arg value="--standard=PEAR" />
            <arg path="${appsource}" />
            <arg path="${libsource}/Tea" />
            <arg path="${basedir}/application" />
        </exec>
    </target>

    <target name="phpcpd" description="Find duplicate code using PHPCPD">
        <exec executable="phpcpd">
            <arg value="--log-pmd" />
            <arg value="${basedir}/build/logs/pmd-cpd.xml" />
            <arg path="${appsource}" />
        </exec>
    </target>

    <target name="phpdox"
        description="Generate API documentation using phpDox">
        <exec executable="phpdox"/>
    </target>

    <target name="phpunit" description="Run unit tests with PHPUnit">
        <exec executable="phpunit" failonerror="true"/>
    </target>

    <target name="phpcb"
        description="Aggregate tool output with PHP_CodeBrowser">
        <exec executable="phpcb">
            <arg value="--log" />
            <arg path="${basedir}/build/logs" />
            <arg value="--source" />
            <arg path="${appsource}" />
            <arg value="--output" />
            <arg path="${basedir}/build/code-browser" />
        </exec>
    </target>

    <target name="fix_tab_indent" description="Change tab indent to 4 space">
        <apply executable="sed" failonerror="false">
            <arg value="-i" />
            <arg value="s/\t/    /g" />
            <fileset dir="${appsource}">
                <include name="**/*.php" />
                <modified />
            </fileset>
            <fileset dir="${libsource}">
                <include name="**/*.php" />
                <modified />
            </fileset>
        </apply>
    </target>

</project>
