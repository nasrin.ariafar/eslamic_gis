<?php
require_once dirname(__FILE__) . '/Bootstrap/Abstract.php';

class BootstrapCli extends Bootstrap_Abstract
{
    protected function _initRoutes()
    {
        $front = Zend_Controller_Front::getInstance();
        $front->setParam('prefixDefaultModule', true);
        $front->setParam('useDefaultControllerAlways', true);
        $front->setDefaultModule('default');
        $front->setDefaultControllerName('cli');
        $front->setDefaultAction('index');
    }

    public function run()
    {
        $front   = $this->getResource('FrontController');
        $default = $front->getDefaultModule();
        if (null === $front->getControllerDirectory($default)) {
            throw new Zend_Application_Bootstrap_Exception(
                'No default controller directory registered with front controller'
            );
        }

        // Set FrontController invoke paramters
        $front->setParams(
            array(
                'bootstrap'      => $this,
                'noViewRenderer' => true
            )
        );

        // Set the Cli Router
        $front->setRouter('Tea_Controller_Router_Cli');
        // Dispatch the request with custom Request and Response classes
        $response = $front->dispatch(
            new Tea_Controller_Request_Cli(),
            new Tea_Controller_Response_Cli()
        );
vaR_dump($response);exit;
        if ($front->returnResponse()) {
            return $response;
        }
    }
}
