
<?php

class Regions_Service_Area extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Regions_Model_DbTable_Areas();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Regions_Model_Area &$area = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }

        $row = $rows->current()->toArray();
        if (!$area instanceof Regions_Model_Area) {
            $area = new Regions_Model_Area();
        }
        $area->fill($row);

        $this->staticSet($id, $area);
        return $area;
    }

    public function getDetailsByPK($id, Regions_Model_Area $area = null)
    {
        $area = $this->getByPK($id);
        if (!$area instanceof Regions_Model_Area) {
            return false;
        }

        $author = Users_Service_User::getInstance()
                ->getByPK($area->getAuthorId());

        $area->setAuthor($author);
        return $area;
    }

    public function getList($filter, $sort)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $cSelect->from(array('c' => 'areas'), array('COUNT(*) AS count'));

        $select->from(array('c' => 'areas'), array('*'));

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                $key = "c.$key";
                $select->where("{$key} = ?", $value);
                $cSelect->where("{$key} = ?", $value);
            }
        }

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "c.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

//        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
//        $count = (int) $rows['count'];

        $areas = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $area = new Regions_Model_Area();
            $area->fill($row);
            $areas[] = $area;
        }

        return $areas;
    }

    public function getLastAreas($objectType, $objectId)
    {
        $cacheId = $objectType . ':' . $objectId . ':lc';
        if ($ret = $this->staticGet($cacheId)) {
            return $ret;
        }

        $filter = array(
            'object_type' => $objectType,
            'object_id' => $objectId
        );
        $sort = array(
            'creation_date' => 'DESC'
        );

        $total = 0;
        $areas = $this->getList($filter, $sort, 0, $total, 5);
        $areas = array_reverse($areas);

        $areas = array(
            'data' => $areas,
            'count' => $total
        );

        $this->staticSet($cacheId, $areas);
        return $areas;
    }

    public function countAreas(Regions_Model_Activity $activity)
    {
        $cSelect = $this->_table->select();
        $cSelect->where("activity_id = ?", $activity->getId());
        $cSelect->from($this->_table, array('COUNT(*) AS count'));

        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        return $count;
    }

    public function save(Regions_Model_Area $area)
    {
        if ($area->isNew()) {
            $data = $area->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $area);
            }
        } else {
            $id = $area->getId();
            $data = $area->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);

            $area = $this->getByPK($pk, $area);

            $this->staticSet($id, $area);

            return $area;
        }

        $cacheId = $area->getObjectType() . ':' . $area->getObjectId() . ':lc';
        $this->staticDel($cacheId);

        return false;
    }

    public function remove(Regions_Model_Area $area)
    {
        $this->staticDel($area->getId());

        $where = $this->_table->getAdapter()->quoteInto('id = ?', $area->getId());
        $this->_table->delete($where);

        $data = array(
            'actor_type' => 'user',
            'actor_id' => $userId,
            'object_id' => $area->getId(),
            'object_type' => 'area',
            'target_type' => $area->getObjectType(),
            'target_id' => $area->getObjectId(),
            'verb' => 'area',
        );
        Tea_Service_Gearman::getInstance()->doBackground('feeds', 'delete_activities', $data);

        Tea_Hook_Registry::dispatchEvent('delete_area', $area);

        $cacheId = $area->getObjectType() . ':' . $area->getObjectId() . ':lc';
        $this->staticDel($cacheId);
    }

    public function removeByConditions($conditions = array())
    {
        $areaLikeService = Regions_Service_AreaLike::getInstance();

        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->getAdapter()->beginTransaction();
        try {
            //--------- delete related tables entries ----------
            $query = 'SELECT id FROM activity_areas WHERE ' . $where;
            $areaLikeService->removeByQuery('area_id', $query);
            //--------------------------------------------------

            $this->_table->delete($where);

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }
    }

    public function removeByQuery($property, $query)
    {
        $areaLikeService = Regions_Service_AreaLike::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        try {
            //--------- delete related tables entries ----------
            switch ($property) {
                case 'id':
                    $areaLikeService->removeByQuery('area_id', $query);
                    break;

                case 'activity_id':
                    $areaLikeService->removeByQuery($property, $query);
                    break;
            }
            //--------------------------------------------------

            $this->_table->delete("$property IN ($query)");

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }
    }

    public function removeByObject($objectType, $objectId)
    {
        $where1 = $this->_table->getAdapter()->quoteInto('object_id   = ?', $objectId);
        $where2 = $this->_table->getAdapter()->quoteInto('object_type = ?', $objectType);
        $where = $where1 . ' AND ' . $where2;
        $this->_table->delete($where);
    }

    public function removeAll()
    {
        $areaLikeService = Regions_Service_AreaLike::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        try {
            //--------- delete related tables entries ----------
            $areaLikeService->removeAll();
            //--------------------------------------------------

            $this->_table->delete('');

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }
    }

    public function getDetails(Regions_Model_Area $area, $user = null)
    {
        $result = $this->_getDetails($area);
        if (!$result) {
            return false;
        }

        $liked = false;
        if ($user instanceof Users_Model_User) {

            $liked = Regions_Service_Like::getInstance()
                    ->hasLike('area', $area->getId(), $user->getId());
        }
        $result['is_liked'] = $liked;

        return $result;
    }

    private function _getDetails(Regions_Model_Area $area)
    {
        $author = Users_Service_User::getInstance()
                ->getByPK($area->getUserId());

        if (!$author instanceof Users_Model_User) {
            return false;
        }

        $author = $author->toArray();
        $return = array(
            'id' => $area->getId(),
            'user_id' => $area->getUserId(),
            'author' => $author,
            'object_type' => $area->getObjectType(),
            'object_id' => $area->getObjectId(),
            'message' => $area->getMessage(),
            'creation_date' => $area->getCreationDate(),
        );

        return $return;
    }

}
