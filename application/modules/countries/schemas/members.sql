SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `members`;
CREATE TABLE IF NOT EXISTS `members` (
    `user_id`         BIGINT(20)        UNSIGNED NOT NULL,
    `group_type`      VARCHAR(40)       NOT NULL,
    `group_id`        BIGINT(20)        UNSIGNED NOT NULL,
    `status`          ENUM('PENDING','ACCEPT','DENIED')  NOT NULL DEFAULT 'ACCEPT',
     `type`           ENUM('USER','POWERUSER','ADMIN')   NOT NULL DEFAULT 'USER',
    `creation_date`   DATETIME          NOT NULL,
    `update_date`     DATETIME          NOT NULL,

    PRIMARY KEY (`user_id`,`group_type`, `group_id`)
) ENGINE=InnoDB;
