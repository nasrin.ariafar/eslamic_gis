SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS comments (
    id              BIGINT(20)	UNSIGNED NOT NULL    AUTO_INCREMENT,
    user_id         BIGINT(20)	UNSIGNED NOT NULL,
    object_type     VARCHAR(40)	NOT NULL,
    object_id       BIGINT(20)	UNSIGNED NOT NULL,
    message         TEXT	    NOT NULL,
    creation_date   DATETIME	NOT NULL,

    PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;
