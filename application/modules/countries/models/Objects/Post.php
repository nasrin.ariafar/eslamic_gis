<?php

class Feeds_Model_Objects_Post
{
    public static function getItem($id)
    {
        $return = null;
        $post   = Posts_Service_Post::getInstance()
            ->getByPK($id);

        if (!$post instanceof Posts_Model_Post) {
            return false;
        }

        $return = Posts_Service_Post::getInstance()->getDetails($post);
        $return = array_merge(
            array (
                'object_type' => 'post',
                'url'         => '/posts#post/' . $post->getId()
            ),
            $return
        );

        return $return;
    }
}
