<?php

class Feeds_Model_Objects_Follow
{
    public static function getItem($id)
    {
        $return = null;
        $follow = Sites_Service_Follow::getInstance()
            ->getByPK($id);

        if ($follow instanceof Sites_Model_Follow) {
            $return = Sites_Service_Follow::getInstance()->getDetails($follow);
            $return = array_merge(
                array (
                    'object_type' => 'follow',
                    'url'         => '/sites#follow/' . $follow->getId()
                ),
                $return
            );
        }

        return $return;
    }
}
