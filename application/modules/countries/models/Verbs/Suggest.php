<?php

class Feeds_Model_Verbs_Suggest
{
    public static function getNotifications($activity)
    {
        $return = array();

        switch (strtolower($activity->getObjectType())) {
        case 'site':
            $site = Sites_Service_Site::getInstance()
                ->getByPK($activity->getObjectId());

            if (!$site instanceof Sites_Model_Site) {
                //TODO error
                return array();
            }

//            if ($activity->getActorId() == $activity->getContextId()) {
//                return;
//            }

            $return[] = array(
                'type'          => 'suggest',
                'to_id'         => $activity->getContextId(),
                'from_id'       => $activity->getActorId(),
                'target_type'   => 'site',
                'target_id'     => $site->getId(),
                'context_type'  => $activity->getContextType(),
                'context_id'    => $activity->getContextId(),
                'creation_date' => $activity->getCreationDate()
            );
            break;
        }

        return $return;
    }
}
