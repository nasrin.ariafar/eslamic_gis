<?php

class Feeds_Model_Verbs_Post
{
    public static function getNotifications($activity)
    {
        $return = array();

        switch (strtolower($activity->getObjectType())) {
        case 'post':
            $post     = Posts_Service_Post::getInstance()
                ->getByPK($activity->getObjectId());
            if (!$post instanceof Posts_Model_Post) {
                //TODO error
                return false;
            }

            if ($activity->getContextType() == 'user') {
                if ($activity->getContextId() == $post->getPosterId()) {
                    return array();
                }
                $return[] = array(
                    'type'          => 'post',
                    'to_id'         => $post->getPosterId(),
                    'from_id'       => $activity->getActorId(),
                    'target_type'   => 'post',
                    'target_id'     => $post->getId(),
                    'context_type'  => 'user',
                    'context_id'    => $activity->getContextId(),
                    'creation_date' => $comment->getCreationDate()
                );
            }

            if ($activity->getContextType() == 'site') {
                $site  = Sites_Service_Site::getInstance()
                    ->getByPK($activity->getContextId());
                $owner = $site->getOwnerId();

                if (   $site->getOwnerId() == $post->getPosterId()
                    || empty($owner)
                ) {
                    return array();
                }

                $return[] = array(
                    'type'          => 'post',
                    'to_id'         => $post->getPosterId(),
                    'from_id'       => $activity->getActorId(),
                    'target_type'   => 'post',
                    'target_id'     => $post->getId(),
                    'context_type'  => 'site',
                    'context_id'    => $activity->getContextId(),
                    'creation_date' => $comment->getCreationDate()
                );
            }

            break;

        case 'comment':
            $comment = Feeds_Service_Comment::getInstance()
                ->getByPK($activity->getObjectId());

            if (!$comment instanceof Feeds_Model_Comment) {
                return;
            }

            switch (strtolower($activity->getTargetType())) {
            case 'post':
                $post = Posts_Service_Post::getInstance()
                    ->getByPK($activity->getTargetId());
                if (!$post instanceof Posts_Model_Post) {
                    //TODO error
                    return false;
                }

                $return[] = array(
                    'type'          => 'comment',
                    'to_id'         => $post->getPosterId(),
                    'from_id'       => $activity->getActorId(),
                    'target_type'   => 'post',
                    'target_id'     => $post->getId(),
                    'creation_date' => $comment->getCreationDate()
                );

            }
            break;
        }

        return $return;
    }
}
