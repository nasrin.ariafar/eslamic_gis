<?php

class Countries_Model_Country extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => null,
        'name' => null,
        'regionId' => null,
        'areaId' => null,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id':
                case 'name':
                case 'regionId':
                case 'areaId':
                case 'creationDate':
                case 'updateDate':
                case 'deleted':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}
