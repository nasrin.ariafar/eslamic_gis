<?php

class Feeds_Worker_PerishFeed
{
    public function work($data)
    {
        $event = $data['event'];
        $object = $data['data'];

        switch ($event) {
        case 'delete_activity':
            if (!$object instanceof Feeds_Model_Activity) {
                return;
            }
            $this->_perishActivity($object);
            break;
        }
    }

    private function _perishActivity($activity)
    {
        $feedService = Feeds_Service_Feed::getInstance();
        
        $feedService->removeByActivity($activity);
    }
}
