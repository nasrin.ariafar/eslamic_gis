<?php

class Reports_Model_Report extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => NULL,
        'description' => NULL,
        'latitude' => NULL,
        'longitude' => NULL,
        'updateDate' => NULL,
        'creationDate' => NULL,
        'tell' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'description' :
                case 'latitude' :
                case 'longitude' :
                case 'updateDate';
                case 'creationDate';
                case 'tell' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

