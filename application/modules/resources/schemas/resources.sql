SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `resources`;
CREATE TABLE IF NOT EXISTS `resources` (
    `id`                BIGINT(20)      UNSIGNED NOT NULL    AUTO_INCREMENT,
    `name`              VARCHAR(256)    NULL,
    `reference_type`    VARCHAR(256)    NOT NULL,
    `reference`         TEXT            NOT NULL,
    `file_size`         INT             NULL,
    `meta_data`         TEXT            NULL,
    `file_type`         VARCHAR(40)     NULL,
    `created_by_id`     BIGINT(20)      UNSIGNED,
    `updated_by_id`     BIGINT(20)      UNSIGNED,
    `creation_date`     DATETIME        NULL,
    `update_date`       DATETIME        NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=2 ;


CREATE TABLE IF NOT EXISTS `house_resources` (
    `house_id`          BIGINT(20)      UNSIGNED NOT NULL,
    `resource_id`       BIGINT(20)      UNSIGNED NOT NULL,

    PRIMARY KEY (`house_id`, `resource_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;
