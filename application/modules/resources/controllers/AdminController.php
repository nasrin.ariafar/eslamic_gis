<?php

class Resources_AdminController extends Tea_Controller_Action
{
    public function init()
    {
       $this->view->moduleLoader()->appendModule('resources-admin');
       $this->view->headLink()->appendStylesheet('css/resources-admin.css');
    }

    public function indexAction()
    {
        
    }
}
