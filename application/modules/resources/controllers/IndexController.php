<?php

class Resources_IndexController extends Tea_Controller_Action
{

    public function init()
    {

    }

    public function indexAction()
    {

    }

    public function getAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $id = $this->_getParam('id');
        $resSrv = Resources_Service_Resource::getInstance();
        $res = $resSrv->getByPK($id);
        if (!$res instanceof Resources_Model_Resource) {
            header("Status: 404 Not Found");
            return;
        }

        $pubPath = realpath(APPLICATION_PATH . '/../public');
        $file = realpath($pubPath . DIRECTORY_SEPARATOR . $res->getLocation());

        if (file_exists($file)) {
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: ' . $res->getLocation());
        } else {
            header("HTTP/1.0 404 Not Found");
            header("Status: 404 Not Found");
        }
    }

}
