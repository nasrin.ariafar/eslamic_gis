<?php

class PlaceTypes_Service_PlaceType extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new PlaceTypes_Model_DbTable_PlaceTypes();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, PlaceTypes_Model_PlaceType $placeType = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$placeType instanceof PlaceTypes_Model_PlaceType) {
            $placeType = new PlaceTypes_Model_PlaceType();
        }
        $placeType->fill($row);
        $placeType->setNew(false);
        return $placeType;
    }

    public function getByName($name)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'place_types'), array('*'));
        $select->where("name=?", $name);

        $placeTypes = array();
        
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new PlaceTypes_Model_PlaceType();
            $item->fill($row);
            $placeTypes[] = $item;
        }

        return $placeTypes;
    }

    public function getList($filter, $sort, &$count)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'place_types'), array('*'));
        $cSelect->from(array('u' => 'place_types'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {

                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;

                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $placeTypes = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new PlaceTypes_Model_PlaceType();
            $item->fill($row);
            $placeTypes[] = $item;
        }

        return $placeTypes;
    }

    public function save(PlaceTypes_Model_PlaceType $placeType)
    {
        if ($placeType->isNew()) {
            $data = $placeType->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $placeType);
            }
        } else {
            $id = $placeType->getId();
            $data = $placeType->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $placeType);
        }

        return false;
    }

    public function remove(PlaceTypes_Model_PlaceType $placeType)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $placeType->getId());
        $this->_table->delete($where);
    }

    public function removeByConditions($conditions = array())
    {
        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->delete($where);
    }

    public function removeByQuery($property, $query)
    {
        $this->_table->delete("$property IN ($query)");
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
