<?php

class PlaceTypes_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted', 0);
        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);
        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($deleted) && $filter["deleted"] = $deleted;

        $placeTypes_service = PlaceTypes_Service_PlaceType::getInstance();
        $placeTypes = $placeTypes_service->getList($filter, $sort, $count);
        $list = array();
        foreach ($placeTypes as $item) {
            $item = $item->toArray();
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'items' => $list
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = PlaceTypes_Service_PlaceType::getInstance();
        $station = $service->getByPK($id);

        if ($station)
            $result = $station->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();

        $id = $params["id"];
        if (!empty($id)) {
            return $this->update($id, $params, $_FILES['file']);
        }

        $service = PlaceTypes_Service_PlaceType::getInstance();

        if (!isset($this->_currentUser)) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody('Unauthorized');
            return;
        }

        $item = $service->getByName($params["name"]);

        if (count($item)) {
            $this->getResponse()
                    ->setHttpResponseCode(409)
                    ->appendBody('Conflict');
            return;
        }

        if (!count($_FILES['file'])) {
            $this->getResponse()
                    ->setHttpResponseCode(400)
                    ->appendBody('Icon in Required!');
            return;
        }

        $placeType = new PlaceTypes_Model_PlaceType();
        $placeType->fill($params);
        $placeType->setIcon($this->saveImage($_FILES['file']));



        $result = PlaceTypes_Service_PlaceType::getInstance()
                ->save($placeType);

        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }


      public function putAction()
    {
        $service = PlaceTypes_Service_PlaceType::getInstance();
        $params = $this->getParams();

        $type = $service->getByPK($params['id']);
        //TO Do
        if (!$type instanceof PlaceTypes_Model_PlaceType) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Place Type)');
            return;
        }

        $type->fill($params);
        $type->setUpdateDate('now');
        $type->setNew(false);
        $result = $service->save($type);

        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($category));
            return;
        }
    }

    protected function update($id, $data, $file)
    {
        $service = PlaceTypes_Service_PlaceType::getInstance();
        $placeType = $service->getByPK($id);
        //TO Do
        if (!$placeType instanceof PlaceTypes_Model_PlaceType) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (PlaceType)');
            return;
        }


        if ($file) {
            unset($data["icon"]);
            $placeType->setIcon($this->saveImage($file));
        }

        $placeType->fill($data);
        $placeType->setUpdateDate('now');
        $placeType->setNew(false);
        $placeType = $service->save($placeType);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($placeType->toArray()));
    }

    public function deleteAction()
    {
        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return;
        }

        $service = PlaceTypes_Service_PlaceType::getInstance();
        $id = $this->_getParam('id');
        $placeType = $service->getByPK($id);

        if (!$placeType instanceof PlaceTypes_Model_PlaceType) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (PlaceType)");
            return;
        }

        try {
            $service->remove($placeType);
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode("Success"));
        } catch (\Exception $e) {
            $this->getResponse()
                    ->setHttpResponseCode(400)
                    ->appendBody(json_encode("Error"));
        }
    }

    protected function saveImage($file)
    {
        $basePath = realpath(APPLICATION_PATH . '/../public');

        if (!file_exists($basePath . '/upfiles/place-types/')) {
            @mkdir($basePath . '/upfiles/place-types/', 0777, true);
        }

        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $path = $basePath . '/upfiles/place-types/';
        $path .= '/' . $timestamp . '_' . $file['name'];

        move_uploaded_file($file['tmp_name'], $path);

        $name = rawurlencode($file['name']);

        $locatin = Zend_Registry::get('config')->app->url . '/upfiles/place-types/' . '/' . $timestamp . '_' . $name;

        return $locatin;
    }

}

