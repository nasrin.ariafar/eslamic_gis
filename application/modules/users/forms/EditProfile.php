<?php

class Users_Form_EditProfile extends Users_Form_Register
{
    protected $_user;
    
    public function __construct(Users_Model_User $user = null)
    {
        parent::__construct();
        
        if (is_object($user)) {
            $this->_user = clone $user;
        }
    }
    
    public function init()
    {
        parent::init();
        
        $id = new Zend_Form_Element('id');
        $id->setRequired(true)
                ->addValidator(new Users_Validate_ExistUser())
                ->addValidator(new Users_Validate_IsCurrentUser());
        $this->addElement($id);
        
        $oldPass = new Zend_Form_Element('old_password');
        $oldPass->setRequired(false)
                ->addFilter(new Zend_Filter_StringTrim());
        $this->addElement($oldPass);
        
        $this->getElement('password')
                ->setRequired(false);
        
        $this->getElement('confirm_password')
                ->setRequired(false);
        
        $this->getElement('profile_id')
                ->addValidator(new Users_Validate_ExistProfile());
    }
    
    public function isValid($data)
    {
        if (isset($data['password'])) {
            $this->getElement('old_password')
                    ->setRequired(true)
                    ->addValidator(new Users_Validate_ValidPassword($this->_user->getPassword()));
            
            $this->getElement('confirm_password')
                    ->setRequired(true);
        }
        
        if (isset($data['email'])) {
            if ($data['email'] == $this->_user->getEmail()) {
                $this->getElement('email')
                        ->clearValidators();
            }
        }
        
        return parent::isValid($data);
    }
}