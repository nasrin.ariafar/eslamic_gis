<?php

class Users_Model_Actions_CreatePost extends Tea_Model_Actions_Abstract
{
    public function render($activity)
    {
        $dt   = $activity->getCreationDate();
        $post = Posts_Service_Post::getInstance()
            ->getByPk($activity->getRefId1());

        if (!$post instanceof Posts_Model_Post) {
            return false;
        }

        if ($dt instanceof DateTime) {
            $dt = $dt->format('Y-m-d H:i:s');
        } else {
            $dt = (string) $dt;
        }

        $content = "";
        $photo   = $post->getPhotoId();
        $video   = $post->getVideoId();  

        if (!empty($photo)) {
            $photo = Resources_Service_Resource::getInstance()->getByPk($photo);
            if ($photo instanceof Resources_Model_Resource) {
                $meta   = $photo->getMetaData();
                $width  = 0;
                $height = 0;

                if (is_array($meta) && isset($meta['width'], $meta['height'])) {
                    if ($meta['width'] > $meta['height']) {
                        $width  = 500;
                        $height = round(($meta['height'] * $width) / $meta['width']);
                    } else {
                        $height = 500;
                        $width  = round(($meta['width'] * $height) / $meta['height']);
                    }

                    $newId = Resources_Service_Resource::getInstance()->resizeImage($photo, $width, $height);
                    if ($newId) {
                        $content .= '<div class="post-thumb-image"><img src="/r/' . $newId . '"/></div>';
                    }
                    if($video){
                         $content .= '<video id = "example_video_1" class = "video-js vjs-default-skin" controls preload = "none" width = "640" height = "264">';
                         $content .= '<source src="/r/' . $video. '?format=mp4" type = "video/mp4" /></video>';
                    }
                }
            }
        }
        $content .= $post->getBody();

        return $content;
    }

    public function getNotificationMessage($activity)
    {
        $actor = $activity->getActor();
        if (!$actor instanceof Users_Model_User) {
            return false;
        }

        return $actor->getFirstName() . ' ' . $actor->getLastname() . ' Posted on your wall';
    }

    public function getFollowers($activity)
    {
        $followService = Users_Service_Follow::getInstance();
        $followersOfFollower  = $followService->getUserFollowersIds($activity->getActorId());
        $followersOfFollowing = $followService->getUserFollowersIds($activity->getObjId());

        $uids = array(
            $activity->getActorId(),
            $activity->getObjId()
        );
        $uids = array_merge($uids, $followersOfFollower);
        $uids = array_merge($uids, $followersOfFollowing);

        return array_unique($uids);
    }

    public function getNotifReceivers($activity)
    {
        $uids = array(
            $activity->getObjId()
        );
        
        $uids = array_unique($uids);
        if (array_search($activity->getActorId(), $uids) !== false) {
            unset($uids[$activity->getActorId()]);
        }
        
        return $uids;
    }
    
    public function sendEmail($activity)
    {
        $userService        = Users_Service_User::getInstance();
        $userPrefService    = Users_Service_Pref::getInstance();
        
        $uids = array(
            $activity->getObjId()
        );
        
        $uids = array_unique($uids);
        if (array_search($activity->getActorId(), $uids) !== false) {
            unset($uids[$activity->getActorId()]);
        }
        
        foreach ($uids as $uid) {
            $user       = $userService->getByPK($uid);
            $userPref   = $userPrefService->getByPK($uid, $activity->getAction());

            if ($userPref instanceof Users_Model_Pref && 
                $userPref->getValue() == 0) {
                
                continue;
            }
            
            $data = array (
                'template' => 'notification',
                'to'       => $user->getEmail(),
                'subject'  => $this->getNotificationMessage($activity),
                'params'   => array (
                    'activity' => $activity
                )
            );
            Tea_Service_Gearman::getInstance()->doBackground('default', 'email', $data);
        }
    }

    public function isLikable()
    {
        return true;
    }

    public function isCommentable()
    {
        return true;
    }

    public function isSharable()
    {
        return true;
    }

    public function isFollowable()
    {
        return true;
    }

    public function isVotable()
    {
        return true;
    }

    public function sendSocialStatus($activity)
    {
        $userService     = Users_Service_User::getInstance();
        $userPrefService = Users_Service_Pref::getInstance();
        $aId   = $activity->getObjId();
        $acted = $userService->getByPK($aId);
        if (!$acted instanceof Users_Model_User) {
            return;
        }

        $user = $userService->getByPK($activity->getActorId());
        if (!$user instanceof Users_Model_User) {
            return false;
        }


        $message  = 'Added a new Post';

        $userPref = $userPrefService->getByPK($user->getId(), 'send_facebook_activity');
        if (!($userPref instanceof Users_Model_Pref) || ($userPref->getValue() == 1)) {
            $params = array(
                'message' => $message,
                'link'    => $acted->getUrl(),
            );

            $picture = $acted->getPhotoUrl();
            if (!empty($picture)) {
                $params['picture'] = $picture;
            }
            $this->sendFacebookFeed($user, $params);
        }

        $userPref = $userPrefService->getByPK($user->getId(), 'send_twitter_activity');
        if (!($userPref instanceof Users_Model_Pref) || ($userPref->getValue() == 1)) {
            $this->tweetInTwitter($user, $message, $acted->getUrl());
        }
    }
}
