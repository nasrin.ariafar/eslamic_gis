<?php

class Users_Model_Country extends Tea_Model_Entity
{
    const PERMISSION_FULL = 1;
    const PERMISSION_WRITE = 2;

    protected $_properties = array(
        'countryId' => null,
        'userId' => null,
        'write' => 0
    );

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'countryId':
                case 'userId':
                case 'write':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}
