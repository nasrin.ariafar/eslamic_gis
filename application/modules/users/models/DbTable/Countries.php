<?php

class Users_Model_DbTable_Countries extends Zend_Db_Table_Abstract
{    
    protected $_name    = 'user_countries';
    protected $_primary =  array('countryId', 'userId');
}