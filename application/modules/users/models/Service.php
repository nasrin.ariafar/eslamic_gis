<?php

class Users_Model_Service extends Tea_Model_Entity
{

    const SERVICE_ANDROID = 0;
    const SERVICE_WEB = 1;
    const SERVICE_WEB_ANDROID = 2;

    protected $_properties = array(
        'id' => null,
        'userId' => null,
        'userRegions' => null,
        'acountDuration' => null,
        'serviceType' => null,
        'balanc' => null,
        'expireDate' => null,
//        "fuzzy_expire_date" => null
    );

    public function __construct($default = true)
    {
        parent::__construct();

        if ($default) {
            $this->setExpireDate('now');
        }
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id':
                case 'userId':
                case 'userRegions':
                case 'acountDuration':
                case 'serviceType':
                case 'balanc':
                case 'expireDate':
                case 'fuzzy_expire_date':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

    public function toArray($forInsert = false)
    {
        $properties = array();

        foreach ($this->_properties as $key => $value) {
            $properties[$key] = $value;
        }

        return $properties;
    }

    public static function hasProperty($property)
    {
        switch ($property) {
            case 'id':
            case 'userId':
            case 'userRegions':
            case 'acountDuration':
            case 'serviceType':
            case 'balanc':
            case 'expireDate':
            case 'fuzzy_expire_date':
                return true;
        }

        return false;
    }

    public function getExpireDate()
    {
        if ($this->_properties['expireDate']) {
            return $this->getLocalDateTime($this->_properties['expireDate'])->format('Y-m-d H:i:s');
        }else {
         return null;
        }
    }

    public function setFuzzyExpireDate($str)
    {
        $this->_properties['fuzzy_expire_date'] = $str;
    }

    public function getFuzzyExpireDate()
    {
        return $this->_properties['fuzzy_expire_date'];
    }

    public function setexpireDate($str)
    {
        $this->_properties['expireDate'] = $str;
    }

}
