<?php

class Users_Model_Token extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => null,
        'userId' => null,
        'token' => null,
        'creationDate' => null
    );

    public function __construct($default = true)
    {
        parent::__construct();

        $this->setCreationDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'userId':
                case 'token':
                case 'creationDate' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}
