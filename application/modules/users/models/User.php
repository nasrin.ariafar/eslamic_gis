<?php

class Users_Model_User extends Tea_Model_Entity
{

    const TYPE_ADMIN = 1;
    const TYPE_NO_ADMIN = 2;
    
    const PERMISSION_FULL = 1;
    const PERMISSION_WRITE = 2;
    const PERMISSION_READ = 3;

//    const READ_PUBLIC = 0;              //view only location public info
//    const READ_PRIVATE = 1;             //view public and private info of location
//    const WRITE_NO_PERMISSION = 0;
//    const WRITE_ONLY = 1;
//    const WRITE_FULL = 2;

    protected $_properties = array(
        'id' => null,
        'username' => null,
        'firstName' => null,
        'lastName' => null,
        'password' => null,
        'email' => null,
        'type' => 2,
        'permission' => null,
        'photo' => null,
        'creationDate' => null,
        'updateDate' => null,
        'deleted' => 0,
        'tell' => null
    );

    public function __construct($default = true)
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'username' :
                case 'firstName' :
                case 'lastName' :
                case 'password' :
                case 'email' :
                case 'tell':
                case 'permission':
                case 'photo' :
                case 'type':
                case 'creationDate' :
                case 'updateDate' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}
