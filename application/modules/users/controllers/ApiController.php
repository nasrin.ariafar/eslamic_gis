<?php

class Users_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        if (!isset($this->_currentUser)) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody('Unauthorized');
            return;
        }

        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);
        $name = $this->_getParam('name');

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($deleted) && $filter["deleted"] = $deleted;
        isset($name) && $filter["name"] = $name;

        $users = Users_Service_User::getInstance()
                ->getList($filter, $sort, $start, $count, $limit);

        $list = array();
        foreach ($users as $user) {
            if ($user->getId() == $this->_currentUser->getId()) {
                $count = $count - 1;
            }else{
                $user_array = $user->toArray();
                $list[] = $user_array;
            }
        }

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode(array(
                            'count' => $count,
                            'items' => $list
                        )));
    }

    public function getAction()
    {
        if (!isset($this->_currentUser)) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody('Unauthorized');
            return;
        }

        $userId = $this->_getParam('id');
        $userService = Users_Service_User::getInstance();
        $user = $userService->getByPK($userId);

        if ($user) {
            $service = Users_Service_Country::getInstance();
            $user_countries = $service->getList(array(
                "userId" => $user->getId()
                    ));

            $countries = array();
            foreach ($user_countries as $country) {
                $countries[] = $country->toArray();
            }


            $Pservice = Users_Service_PlaceType::getInstance();
            $user_types = $Pservice->getList(array(
                "userId" => $user->getId()
                    ));

            $types = array();
            foreach ($user_types as $type) {
                $types[] = $type->toArray();
            }


            $user = $user->toArray();
            $user["placeTypes"] = $types;
            $user["countries"] = $countries;

            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($user));
        } else {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody(json_encode("Not Found User"));
        }
    }

    public function postAction()
    {
        if (!isset($this->_currentUser)) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody('Unauthorized');
            return;
        }

        $user_service = Users_Service_User::getInstance();

        $params = $this->getParams();
        $username = $params['username'];
        $password = $params['password'];
        $email = $params['email'];
        $permission = $params['permission'];
        $params["id"] && $user = Users_Service_User::getInstance()->getByPk($params["id"]);

        if ($permission == Users_Model_User::PERMISSION_WRITE) {
            isset($user) && $this->removeAllUserCountries($user);
            $writeOnlyCountries = $params["writeOnlyCountries"];
            $wirteFullCountries = $params["wirteFullCountries"];
            isset($writeOnlyCountries) && $writeOnlyCountries = explode(',', $writeOnlyCountries);
            isset($wirteFullCountries) && $wirteFullCountries = explode(',', $wirteFullCountries);
        } else {
            isset($user) && $this->removeAllUserPlaceTypes($user);
            $publicPlaces = $params["publicPlaces"];
            $privatePlaces = $params["privatePlaces"];
            isset($publicPlaces) && $publicPlaces = explode(',', $publicPlaces);
            isset($privatePlaces) && $privatePlaces = explode(',', $privatePlaces);
        }

        $user = new Users_Model_User();

        if (!isset($params["id"])) {
            $user->setNew(true);
            $u = $user_service->getByEmail($email);
            if (isset($u)) {
                $this->getResponse()
                        ->setHttpResponseCode(401)
                        ->appendBody("پست الکترونیکی تکراری است.‏");
                return;
            }

            $u = $user_service->getByUserName($username);
            if (isset($u)) {
                $this->getResponse()
                        ->setHttpResponseCode(401)
                        ->appendBody("نام کاربری تکراری است.‏");
                return;
            }
        } else {
            $user->setNew(false);
        }


        $user->fill($params);

        if (isset($_FILES['file'])) {
            $user->setPhoto($this->saveImage($_FILES['file']));
        }

//        if (!$params["id"] && !count($_FILES['file'])) {
//            $this->getResponse()
//                    ->setHttpResponseCode(400)
//                    ->appendBody('انتخاب عکس اجباری است!‏');
//            return;
//        }

        $user = Users_Service_User::getInstance()
                ->save($user);

        if ($user) {

            $this->removeAllUserCountries($user);
            $this->removeAllUserPlaceTypes($user);

            if (count($writeOnlyCountries) > 0) {
                $service = Users_Service_Country::getInstance();
                $service->addUserCountries($user, $writeOnlyCountries, Users_Model_Country::PERMISSION_WRITE);
            }

            if (count($wirteFullCountries) > 0) {
                $service = Users_Service_Country::getInstance();
                $service->addUserCountries($user, $wirteFullCountries, Users_Model_Country::PERMISSION_FULL);
            }

            if (count($privatePlaces) > 0) {
                $service = Users_Service_PlaceType::getInstance();
                $service->addUserPlaceTypes($user, $privatePlaces, Users_Model_PlaceType::ACCESS_PRIVATE);
            }

            if (count($publicPlaces) > 0) {
                $service = Users_Service_PlaceType::getInstance();
                $service->addUserPlaceTypes($user, $publicPlaces, Users_Model_PlaceType::ACCESS_PUBLIC);
            }

            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($user->toArray()));
        }
    }

    public function putAction()
    {

        if (!isset($this->_currentUser)) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody('Unauthorized');
            return;
        }

        $params = $this->getParams();

        $user = $this->_currentUser;

        if ($params['old']) {
            if ($user->getPassword() != $params['old']) {
                $this->getResponse()
                        ->setHttpResponseCode(401)
                        ->appendBody("رمز عبور قبلی اشتباه است");
                return;
            }
        }

        $user->fill($params);
        $user->setNew(false);

        if (Users_Service_User::getInstance()->save($user)) {
            $user = $user->toArray(false, null, array('password'));

            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode("Success"));
        } else {
            $this->getResponse()
                    ->setHttpResponseCode(400)
                    ->appendBody("User Not modified");
        }
    }

    public function deleteAction()
    {
        $userService = Users_Service_User::getInstance();

        $user = $userService->getByPK($this->_getParam('id'));
        if (!$user instanceof Users_Model_User) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found User");
            return;
        }

        $places = Places_Service_Place::getInstance()->getList(array('createBy'=>$user->getId()), null, null, $count, null);
        if(count($places) > 0){
            $this->getResponse()
                ->setHttpResponseCode(403)
                ->appendBody(json_encode(array("message"=>"ابتدا مکان‌هایی را که توسط کاربر ثبت شده است حذف نمایید")));
            return;
        }

        $places = Places_Service_Place::getInstance()->getList(array('acceptBy'=>$user->getId()), null, null, $count, null);
        if(count($places) > 0){
            $this->getResponse()
                ->setHttpResponseCode(403)
                ->appendBody(json_encode(array("message"=>"ابتدا مکان‌هایی را که توسط کاربر تایید شده است حذف نمایید")));
            return;
        }



        Users_Service_Token::getInstance()->removeUserToken($user->getId());
        Users_Service_Country::getInstance()->removeUserCountries($user->getId());
        Users_Service_PlaceType::getInstance()->removeUserPlaceTypes($user->getId());

        $userService->remove($user);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode(array("status"=>"ok")));
    }

    protected function saveImage($file)
    {
        $basePath = realpath(APPLICATION_PATH . '/../public');

        if (!file_exists($basePath . '/upfiles/user-photos/')) {
            @mkdir($basePath . '/upfiles/place-types/', 0777, true);
        }

        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $path = $basePath . '/upfiles/place-types/';
        $path .= '/' . $timestamp . '_' . $file['name'];

        move_uploaded_file($file['tmp_name'], $path);

        $name = rawurlencode($file['name']);

        $locatin = '/upfiles/place-types/' . '/' . $timestamp . '_' . $name;

        return $locatin;
    }

    protected function removeAllUserCountries($user)
    {
        $service = Users_Service_Country::getInstance();
        $service->deleteAllUserCountries($user);
    }

    protected function removeAllUserPlaceTypes($user)
    {
        $service = Users_Service_PlaceType::getInstance();
        $service->deleteAllPlaceTypes($user);
    }

}
