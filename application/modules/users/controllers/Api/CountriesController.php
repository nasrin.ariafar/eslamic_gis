<?php

class Users_Api_CountriesController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {

        $authService = Users_Service_Auth::getInstance();
        $user = $authService->getCurrentUser();

        $params = $this->_getAllParams();
        $userId = $this->_getParam('id');

        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return;
        }


        if ($user->getType() == Users_Model_User::TYPE_ADMIN) {
            $service = Countries_Service_Country::getInstance();
            $list = $service->getList(array());
            $countries = array();
            foreach ($list as $item){
                $countries[] = $item->toArray();
            }
        }else {
            $service = Users_Service_Country::getInstance();
            $countries = $service->getUserCountries($userId);
        }



        $response = array(
            'items' => $countries
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

}
