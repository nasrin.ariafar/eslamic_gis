<?php

class Users_Validate_IsCurrentUser extends Zend_Validate_Abstract
{
    const NOT_CURRENT_USER = 'notCurrentUser';
    
    protected $_messageTemplates = array(
        self::NOT_CURRENT_USER => "User is unauthorized",
    );
    
    public function isValid($value)
    {
        $authService = Users_Service_Auth::getInstance();
        
        if ($value == 'me' || $value == '0') {
            return true;
        } 
        
        $user = $authService->getCurrentUser();
        if (!$user instanceof Users_Model_User) {
            $this->_error(self::NOT_CURRENT_USER);
            return false;
        }
        if($user->getId() != $value) {
            $this->_error(self::NOT_CURRENT_USER);
            return false;
        }
        
        return true;
    }
}
