
ALTER TABLE users;
-- ADD FOREIGN KEY (`profile_id`)      REFERENCES dept_profiles(`id`),
-- ADD FOREIGN KEY (`photo_id`)        REFERENCES resources(`id`),
-- ADD FOREIGN KEY (`created_by_id`)   REFERENCES users(`id`),
-- ADD FOREIGN KEY (`updated_by_id`)   REFERENCES users(`id`);


ALTER TABLE user_profiles;
-- ADD FOREIGN KEY (`created_by_id`)   REFERENCES users(`id`),
-- ADD FOREIGN KEY (`updated_by_id`)   REFERENCES users(`id`);

ALTER TABLE user_prefs
ADD FOREIGN KEY (`user_id`)         REFERENCES users(`id`),
ADD FOREIGN KEY (`created_by_id`)   REFERENCES users(`id`),
ADD FOREIGN KEY (`updated_by_id`)   REFERENCES users(`id`);


ALTER TABLE user_networks
ADD FOREIGN KEY (`user_id`)         REFERENCES users(`id`),
ADD FOREIGN KEY (`created_by_id`)   REFERENCES users(`id`),
ADD FOREIGN KEY (`updated_by_id`)   REFERENCES users(`id`);


ALTER TABLE user_roles
ADD FOREIGN KEY (`user_id`)         REFERENCES users(`id`);
-- ADD FOREIGN KEY (`role_id`)         REFERENCES roles(`id`),


ALTER TABLE user_permissions
ADD FOREIGN KEY (`user_id`)         REFERENCES users(`id`);
-- ADD FOREIGN KEY (`role_id`)         REFERENCES roles(`id`),
-- ADD FOREIGN KEY (`context_id`)      REFERENCES corp_contexts(`id`),
-- ADD FOREIGN KEY (`action_id`)       REFERENCES context_actions(`id`);


ALTER TABLE user_collections
ADD FOREIGN KEY (`user_id`)         REFERENCES users(`id`);