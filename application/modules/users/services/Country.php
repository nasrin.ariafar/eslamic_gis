<?php

class Users_Service_Country extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Users_Model_DbTable_Countries();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($country_id, $user_id, Users_Model_Country $obj = null)
    {
        $rows = $this->_table->find($country_id, $user_id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        $obj = new Users_Model_Country();
        $obj->fill($row);
        $obj->setNew(false);
        return $obj;
    }

    public function getList($filter)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $select->from(array('u' => 'user_countries'), array('*'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                }
            }
        }

        $select->limit($limit, $start);

        $users = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $user = new Users_Model_Country();
            $user->fill($row);
            $users[] = $user;
        }

        return $users;
    }

    public function getUserCountries($userId)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $cSelect->from(array('f' => 'user_countries'));
        $cSelect->where("userId = '$userId'");
        $cSelect->joinLeft(
                array('c' => 'countries'), "f.countryId = c.id", array(
            'name' => 'name',
            'id' => 'id'
                )
        );


        $rows = $this->_table->getAdapter()->fetchAll($cSelect);

        return $rows;
    }

    public function addUserCountries($user, $countries, $permission)
    {
        foreach ($countries as $id) {
            $model = new Users_Model_Country();
            $model->fill(array(
                "countryId" => $id,
                "userId" => $user->getId(),
                "write" => $permission
            ));

            if ($this->getByPK($user->getId(), $id)) {
                $model->setNew(false);
            }

            $this->save($model);
        }
    }

    public function save(Users_Model_Country $obj)
    {
        if ($obj->isNew()) {
            $data = $obj->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($obj->getUserId(), $obj->getCountryId(), $obj);
            }
        } else {
            $id = $profile->getId();
            $data = $profile->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $profile);
        }

        return false;
    }

    public function remove(Users_Model_Country $profile)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $profile->getId());
        $this->_table->delete($where);
    }

    public function deleteAllUserCountries(Users_Model_User $user)
    {
        $where = $this->_table->getAdapter()->quoteInto('userId = ?', $user->getId());
        $this->_table->delete($where);
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

    public function removeUserCountries($userId)
    {
        $where = $this->_table->getAdapter()->quoteInto('userId = ?', $userId);
        $this->_table->delete($where);
    }

}
