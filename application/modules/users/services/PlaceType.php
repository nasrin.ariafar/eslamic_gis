<?php

class Users_Service_PlaceType extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Users_Model_DbTable_PlaceTypes();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($placeType_id, $user_id, Users_Model_PlaceType $obj = null)
    {
        $rows = $this->_table->find($placeType_id, $user_id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        $obj = new Users_Model_PlaceType();
        $obj->fill($row);
        $obj->setNew(false);
        return $obj;
    }

    public function getList($filter)
    {
        $select = $this->_table->select();

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                $select->where("{$key} = ?", $value);
            }
        }

//        if (is_array($sort)) {
//            foreach ($sort as $key => $ord) {
//                $select->order($key . ' ' . strtoupper($ord));
//            }
//        }
        $cSelect = clone $select;
        $cSelect->from($this->_table, array('COUNT(*) AS count'));
        $rows = $this->_table->fetchRow($cSelect);
//        $count = (int)$rows['count'];
//        $select->limit($limit);

        $result = array();
        foreach ($this->_table->fetchAll($select) as $row) {
            $profile = new Users_Model_PlaceType();
            $profile->fill($row);
            $profile->setNew(false);
            $result[] = $profile;
        }

        return $result;
    }

    public function getUserCountries($userId)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $cSelect->from(array('f' => 'user_regions'));
        $cSelect->where("userId = '$userId'");
        $cSelect->join(array('s' => 'regions'), 'f.regionCode = s.code', array())
                ->joinLeft(
                        array('ur' => 'regions'), "f.regionCode = ur.code", array(
                    'id' => 'id',
                    'code' => 'code',
                    'name' => 'name',
                        )
        );


        $rows = $this->_table->getAdapter()->fetchAll($cSelect);

        $list = array();
        foreach ($rows as $row) {
            $region = new Users_Model_PlaceType();
            $region->fill($row);
            $region->setNew(false);

            $list[] = $region;
        }

        return $list;
    }

    public function addUserPlaceTypes($user, $placeTypes, $access)
    {
        foreach ($placeTypes as $id) {
            $data = $this->getByPK($user->getId(), $id);
            if (!$data) {
                $model = new Users_Model_PlaceType();
                $model->fill(array(
                    "placeTypeId" => $id,
                    "userId" => $user->getId(),
                    "access" => $access
                ));

                $this->save($model);
            }else{
                var_dump($data);exit;
            }
        }
    }

    public function save(Users_Model_PlaceType $obj)
    {
        if ($obj->isNew()) {
            $data = $obj->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($obj->getUserId(), $obj->getPlaceTypeId(), $obj);
            }
        } else {
            $id = $profile->getId();
            $data = $profile->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $profile);
        }

        return false;
    }

    public function remove(Users_Model_PlaceType $profile)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $profile->getId());
        $this->_table->delete($where);
    }

    public function deleteAllPlaceTypes(Users_Model_User $user)
    {
        $where = $this->_table->getAdapter()->quoteInto('userId = ?', $user->getId());
        $this->_table->delete($where);
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

    public function removeUserPlaceTypes($userId)
    {
        $where = $this->_table->getAdapter()->quoteInto('userId = ?', $userId);
        $this->_table->delete($where);
    }

}
