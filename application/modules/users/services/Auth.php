<?php

class Users_Service_Auth extends Tea_Service_Abstract
{

    private static $_instance = null;
    protected $auth;
    protected $adapter;

    private function __construct()
    {
        $this->auth = Zend_Auth::getInstance();
        $this->adapter = new Zend_Auth_Adapter_DbTable(Zend_Registry::get('db'));
        $this->adapter->setTableName('users')
                ->setIdentityColumn('username')
                ->setCredentialColumn('password');
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function authenticate($username, $password)
    {
        $this->adapter->setIdentity($username);
        $this->adapter->setCredential($password);

        return $this->auth->authenticate($this->adapter)->isValid();
    }

    public function saveCurrentUser($user=null, $newLogin=true)
    {
        if (!$user instanceof Users_Model_User) {
            $doc = $this->adapter->getResultRowObject();
            $user = new Users_Model_User();
            $user->fill((array)$doc);
            $user->setNew(false);
        }

        $this->auth->getStorage()->write($user);

        if ($newLogin) {
            $user->setLastLoginDate('now');
            Users_Service_User::getInstance()->save($user);
        }
    }


    public function getCurrentUser()
    {
        return $this->auth->getStorage()->read();
    }

    public function clearCurrentUser()
    {
        return Zend_Auth::getInstance()->clearIdentity();
    }

}
