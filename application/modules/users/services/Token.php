<?php

class Users_Service_Token extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Users_Model_DbTable_Tokens();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Users_Model_Token &$user = null)
    {
        if ($ret = $this->staticGet($id)) {
            if ($user instanceof Users_Model_Token) {
                $user = $ret;
            }

            return $ret;
        }

        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$user instanceof Users_Model_Token) {
            $user = new Users_Model_Token();
        }
        $user->fill($row);
        $user->setNew(false);

        $this->staticSet($id, $user);
        return $user;
    }

    public function getByToken($token)
    {
        $select = $this->_table->select();
        $select->where("token = ?", $token);
        $row = $this->_table->getAdapter()->fetchRow($select);
        if ($row) {
            $user_token = new Users_Model_Token();
            $user_token->fill($row);
            $user_token->setNew(false);
            return $user_token;
        }
        return;
    }

    public function save(Users_Model_Token $obj)
    {
        if ($obj->isNew()) {
            $data = $obj->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                $this->getByPK($pk, $obj);

                $this->staticSet($id, $obj);

                return $obj;
            }
        } else {
            $id = $obj->getId();
            $this->staticDel($id);
            $data = $obj->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);

            $this->getByPK($id, $obj);
            return $obj;
        }

        return false;
    }

    public function removeUserToken($userId)
    {
        $where = $this->_table->getAdapter()->quoteInto('userId = ?', $userId);
        $this->_table->delete($where);
    }
}
