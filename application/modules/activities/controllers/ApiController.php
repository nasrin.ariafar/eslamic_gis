<?php

class Activities_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $itemId = $this->_getParam('itemId');
        $userId = $this->_getParam('userId');

        $orderBy = $this->_getParam('order_by', 'creationDate');
        $orderDir = $this->_getParam('order_dir', 'desc');

        $sort = array($orderBy => $orderDir);

        $activities_service = Activities_Service_Activity::getInstance();

        $filter = array();

        isset($deleted) && $filter["deleted"] = $deleted;
        isset($itemId) && $filter['itemId'] = $itemId;
        isset($userId) && $filter['userId'] = $userId;
        $activities = $activities_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        
        foreach ($activities as $item) {
            $item = $item->toArray();
            $user_service = Users_Service_User::getInstance();
            $user = $user_service->getByPK($item['userId']);
            unset($item['userId']);
            unset($item['itemId']);
            $item['user'] = $user->toArray();
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Activities_Service_Activity::getInstance();
        $activity = $service->getByPK($id);

        if ($activity)
            $result = $activity->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();
//        if (!isset($user)) {
//            $this->getResponse()
//                    ->setHttpResponseCode(404)
//                    ->appendBody('Unauthorized');
//            return;
//        }

        $activity = new Activities_Model_Activity();
        $activity->setupdateDate('now');
        $activity->fill($params);
        $result = Activities_Service_Activity::getInstance()
                ->save($activity);
        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = Activities_Service_Activity::getInstance();
        $params = $this->getParams();

        $activity = $service->getByPK($params['id']);
        //TO Do
        if (!$activity instanceof Activities_Model_Activity) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Activity)');
            return;
        }

        $activity->fill($params);
        $activity->setUpdateDate('now');
        $activity->setNew(false);
        $activity = $service->save($activity);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($activity->toArray()));
    }

    public function deleteAction()
    {
        $service = Activities_Service_Activity::getInstance();
        $activityId = $this->_getParam('id');
        $activity = $service->getByPK($activityId);

        if (!$activity instanceof Activities_Model_Activity) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Activity)");
            return;
        }

        $service->remove($activity);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}

