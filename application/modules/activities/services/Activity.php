<?php

class Activities_Service_Activity extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Activities_Model_DbTable_Activities();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Activities_Model_Activity $activity = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$activity instanceof Activities_Model_Activity) {
            $activity = new Activities_Model_Activity();
        }
        $activity->fill($row);
        $activity->setNew(false);
        return $activity;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'activities'), array('*'));
        $cSelect->from(array('u' => 'activities'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'creationDate':
                        $select->where("u.creationDate > '$value'");
                        $cSelect->where("u.creationDate > '$value'");
                        break;

                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                        break;
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $activities = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new Activities_Model_Activity();
            $item->fill($row);
            $activities[] = $item;
        }

        return $activities;
    }

    public function save(Activities_Model_Activity $activity)
    {
        if ($activity->isNew()) {
            $data = $activity->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $activity);
            }
        } else {
            $id = $activity->getId();
            $data = $activity->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $activity);
        }

        return false;
    }

    public function remove(Activities_Model_Activity $activity)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $activity->getId());
        $this->_table->delete($where);
    }

    public function removeByConditions($conditions = array())
    {
        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->delete($where);
    }

    public function removeByQuery($property, $query)
    {
        $this->_table->delete("$property IN ($query)");
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
