<?php

class Regions_Service_Region extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Regions_Model_DbTable_Regions();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Regions_Model_Region $region = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$region instanceof Regions_Model_Region) {
            $region = new Regions_Model_Region();
        }
        $region->fill($row);
        $region->setNew(false);
        return $region;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'regions'), array('*'));
        $cSelect->from(array('u' => 'regions'), array('COUNT(*) AS count'));

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                $select->where("{$key} = ?", $value);
            }
        }

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $regions = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new Regions_Model_Region();
            $item->fill($row);
            $regions[] = $item;
        }

        return $regions;
    }

    public function save(Regions_Model_Region $region)
    {
        if ($region->isNew()) {
            $data = $region->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $region);
            }
        } else {
            $id = $region->getId();
            $data = $region->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $region);
        }

        return false;
    }

    public function remove(Regions_Model_Region $region)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $region->getId());
        $this->_table->delete($where);
    }

    public function removeByConditions($conditions = array())
    {
        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->delete($where);
    }

    public function removeByQuery($property, $query)
    {
        $this->_table->delete("$property IN ($query)");
    }

    public function removeByActivity(Regions_Model_Activity $activity)
    {
        $where = $this->_table->getAdapter()->quoteInto('activity_id = ?', $activity->getId());
        $this->_table->delete($where);
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
