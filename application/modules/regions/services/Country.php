<?php

class Regions_Service_Country extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Regions_Model_DbTable_Countries();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Regions_Model_Country $country = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$country instanceof Regions_Model_Country) {
            $country = new Regions_Model_Country();
        }
        $country->fill($row);
        $country->setNew(false);
        return $country;
    }

    public function getList($filter, $sort)
    {
        $select = $this->_table->select();

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                $select->where("{$key} = ?", $value);
            }
        }

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $select->order($key . ' ' . strtoupper($ord));
            }
        }
        $cSelect = clone $select;
        $cSelect->from($this->_table, array('COUNT(*) AS count'));
//        $rows = $this->_table->fetchRow($cSelect);
//        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $result = array();
        foreach ($this->_table->fetchAll($select) as $row) {
            $country = new Regions_Model_Country();
            $country->fill($row);
            $country->setNew(false);
            $result[] = $country;
        }

        return $result;
    }

    public function countCountries($objectType, $objectId)
    {
        $cSelect = $this->_table->select();
        $cSelect->where("object_type = ?", $objectType);
        $cSelect->where("object_id   = ?", $objectId);
        $cSelect->from($this->_table, array('COUNT(*) AS count'));

        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        return $count;
    }

    public function save(Regions_Model_Country $country)
    {
        if ($country->isNew()) {
            $data = $country->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $country);
            }
        } else {
            $id = $country->getId();
            $data = $country->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $country);
        }

        return false;
    }

    public function remove(Regions_Model_Country $country)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $country->getId());
        $this->_table->delete($where);
    }

    public function removeByObject($objectType, $objectId)
    {
        $where1 = $this->_table->getAdapter()->quoteInto('object_id   = ?', $objectId);
        $where2 = $this->_table->getAdapter()->quoteInto('object_type = ?', $objectType);
        $where = $where1 . ' AND ' . $where2;
        $this->_table->delete($where);
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

    public function hasCountry($objectType, $objectId, $userId)
    {
        $country = $this->getByPK($userId, $objectType, $objectId);
        return ($country instanceof Regions_Model_Country);
    }

    public function uncountry($objectType, $objectId, $userId)
    {
        $country = $this->getByPK($userId, $objectType, $objectId);
        if ($country instanceof Regions_Model_Country) {
            $this->remove($country);
        }
    }

    public function getCountryrs($objectType, $objectId, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->select();
        $select->where('object_type = ?', $objectType);
        $select->where('object_id   = ?', $objectId);

        $cSelect = clone $select;
        $cSelect->from($this->_table, array('COUNT(*) AS count'));
        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $select->order($key . ' ' . strtoupper($ord));
            }
        }
        $select->limit($limit, $start);

        $ids = array();
        foreach ($this->_table->fetchAll($select) as $row) {
            $ids[] = $row['user_id'];
        }

        return Users_Service_User::getInstance()->getByPKs($ids);
    }

    public function getCountrydIds($userId, $objectType, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->select();
        $select->where('object_type = ?', $objectType);
        $select->where('user_id     = ?', $userId);

        $cSelect = clone $select;
        $cSelect->from($this->_table, array('COUNT(*) AS count'));
        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $select->order($key . ' ' . strtoupper($ord));
            }
        }
        $select->limit($limit, $start);

        $ids = array();
        foreach ($this->_table->fetchAll($select) as $row) {
            $ids[] = $row['object_id'];
        }

        return $ids;
    }

}
