<?php

class Feeds_Hook_PublishFeed
{
    public function execute($event, $data)
    {
        $data = array(
            'event' => $event,
            'data'  => $data
        );

        Tea_Service_Gearman::getInstance()->doBackground('feeds', 'publish_feed', $data);
    }
}
