<?php

class Regions_Model_DbTable_Areas extends Zend_Db_Table_Abstract
{
    protected $_name    = 'areas';
    protected $_primary = 'id';
}
