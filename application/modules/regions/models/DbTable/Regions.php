<?php

class Regions_Model_DbTable_Regions extends Zend_Db_Table_Abstract
{
    protected $_name    = 'regions';
    protected $_primary = 'id';
}
