<?php

class Regions_Model_Region extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => null,
        'name' => null,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setupdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id':
                case 'name':
                case 'creationDate':
                case 'updateDate':
                case 'deleted':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}
