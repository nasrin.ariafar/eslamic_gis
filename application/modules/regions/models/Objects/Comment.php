<?php

class Feeds_Model_Objects_Comment
{
    public static function getItem($id)
    {
        $return  = null;
        $comment = Feeds_Service_Comment::getInstance()
            ->getByPK($id);

        if (!$comment instanceof Feeds_Model_Comment) {
            return null;
        }

        $return = $comment->toArray();
        $return['object_type'] = 'comment';

        return $return;
    }
}
