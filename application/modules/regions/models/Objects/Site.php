<?php

class Feeds_Model_Objects_Site
{
    public static function getItem($id)
    {
        $return = null;
        $site   = Sites_Service_Site::getInstance()
            ->getByPK($id);

        if ($site instanceof Sites_Model_Site) {
            $return = Sites_Service_Site::getInstance()->getDetails($site);
            $return = array_merge(
                array (
                    'object_type' => 'site',
                    'url'         => '/sites#profile/' . $site->getId()
                ),
                $return
            );
        }

        return $return;
    }
}
