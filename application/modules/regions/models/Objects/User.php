<?php

class Feeds_Model_Objects_User
{
    public static function getItem($id)
    {
        $return = null;
        $user   = Users_Service_User::getInstance()
            ->getByPK($id);

        if ($user instanceof Users_Model_User) {
            $return = $user->toArray();
            unset($return['password']);
            $return['object_type'] = 'user';
            $return['url']         = '/users#profile/' . $user->getId();
        }

        return $return;
    }
}
