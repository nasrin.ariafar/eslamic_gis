<?php

class Feeds_Model_Verbs_Like
{
    public static function getNotifications($activity)
    {
        $return = array();

        switch (strtolower($activity->getObjectType())) {
        case 'post':
            $post     = Posts_Service_Post::getInstance()
                ->getByPK($activity->getObjectId());

            if (!$post instanceof Posts_Model_Post) {
                //TODO error
                return array();
            }

            if ($activity->getActorId() == $post->getPosterId()) {
                return;
            }

            $return[] = array(
                'type'          => 'like',
                'to_id'         => $post->getPosterId(),
                'from_id'       => $activity->getActorId(),
                'target_type'   => 'post',
                'target_id'     => $post->getId(),
                'context_type'  => $activity->getContextType(),
                'context_id'    => $activity->getContextId(),
                'creation_date' => $post->getCreationDate()
            );
            break;
        }

        return $return;
    }
}
