<?php

class Regions_Api_AreasController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $activityService = Regions_Service_Activity::getInstance();
        $areaService = Regions_Service_ActivityArea::getInstance();

        $limit = $this->_getParam('limit', 30);
        $start = $this->_getParam('start', 0);
        $orderBy = $this->_getParam('order_by', 'creation_date');
        $orderDir = $this->_getParam('order_dir', 'asc');

        $activityId = $this->_getParam('id');
        $activity = $activityService->getByPK($activityId);
        if (!$activity instanceof Regions_Model_Activity) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Activity)");
            return;
        }

        $filter = array(
            'activity_id' => $activity->getId()
        );

        $sort = array($orderBy => $orderDir);

        $areas = $areaService->getList($filter, $sort, $start, $count, $limit, $this->_currentUser);

        $list = array();
        foreach ($areas as $area) {
            $_area = $area->toArray();
            $_area['author'] = $area->getAuthor()->toArray();
            $_area['is_liked'] = $area->getIsLiked();

            $list[] = $_area;
        }

        $result = array(
            'count' => $count,
            'list' => $list
        );

        $response = json_encode($result);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

    public function getAction()
    {
        $activityService = Regions_Service_Activity::getInstance();
        $areaService = Regions_Service_Area::getInstance();

        $activityId = $this->_getParam('id');
        $activity = $activityService->getByPK($activityId);
        if (!$activity instanceof Regions_Model_Activity) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Activity)");
            return;
        }

        $areaId = $this->_getParam('sub_id');
        $area = $areaService->getDetailsByPK($areaId, null, $this->_currentUser);
        if (!$area instanceof Regions_Model_ActivityArea) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Area)");
            return;
        }
        if ($area->getActivityId() != $activity->getId()) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Area Not For This Activity");
            return;
        }

        $author = $area->getAuthor();
        if (!$author instanceof Users_Model_User) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Author)");
            return;
        }

        $_area = $area->toArray();
        $_area['author'] = $author->toArray();
        $_area['is_liked'] = $area->getIsLiked();

        $response = json_encode($_area);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

    public function postAction()
    {
        $regionsService = Regions_Service_Region::getInstance();
        $areaService = Regions_Service_Area::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return;
        }

        $regionId = $this->_getParam('id');
        $region = $regionsService->getByPK($regionId);
        if (!$region instanceof Regions_Model_Region) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Region)");
            return;
        }

        $params = $this->getParams();
        unset($params['id']);

        $area = new Regions_Model_Area();
        $area->fill($params);
        $area->setRegionId($region->getId());
        $area->setNew(true);
        $_area = $areaService->save($area);
        if ($_area) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($_area->toArray()));
        }
    }

    public function deleteAction()
    {
        $regionService = Regions_Service_Region::getInstance();
        $areaService = Regions_Service_Area::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return;
        }

        $regionId = $this->_getParam('id');
        $region = $regionService->getByPK($regionId);
        if (!$region instanceof Regions_Model_Region) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Region)");
            return;
        }

        $areaId = $this->_getParam('sub_id');
        $area = $areaService->getByPK($areaId);
        if (!$area instanceof Regions_Model_Area) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Area)");
            return;
        }
        if ($area->getRegionId() != $region->getId()) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Area Not For This Region");
            return;
        }

        try {
            $areaService->remove($area);
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode("Success"));
        } catch (\Exception $e) {
            $this->getResponse()
                    ->setHttpResponseCode(400)
                    ->appendBody(json_encode("Error"));
        }
    }

    public function putAction()
    {

        $regionService = Regions_Service_Region::getInstance();
        $areaService = Regions_Service_Area::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return;
        }

        $regionId = $this->_getParam('id');
        $region = $regionService->getByPK($regionId);
        if (!$region instanceof Regions_Model_Region) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Region)");
            return;
        }

        $areaId = $this->_getParam('sub_id');
        $area = $areaService->getByPK($areaId);
        if (!$area instanceof Regions_Model_Area) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Area)");
            return;
        }
        if ($area->getRegionId() != $region->getId()) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Area Not For This Region");
            return;
        }

        $params = $this->getParams();
        $area->fill($params);
        $area->setUpdateDate('now');
        $area->setNew(false);
        $service = $areaService->save($area);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($area->toArray()));
    }

}
