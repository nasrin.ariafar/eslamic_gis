SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `votes`;
CREATE TABLE IF NOT EXISTS votes (
    id             BIGINT(20)  UNSIGNED NOT NULL AUTO_INCREMENT,
    object_type    VARCHAR(40) NOT NULL,
    object_id      BIGINT(20)  UNSIGNED NOT NULL,
    voter_id       BIGINT(20)  UNSIGNED NOT NULL,
    type           ENUM('up','down','spam','rate') NOT NULL,
    value          INT(1)          ,
    creation_date  DateTime    NOT NULL,
    update_date    DATETIME    NOT NULL,

    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `vote_cache`;
CREATE TABLE IF NOT EXISTS vote_cache (
    id            BIGINT(20)    UNSIGNED NOT NULL AUTO_INCREMENT,
    object_type   VARCHAR(40) NOT NULL,
    object_id     BIGINT(20)    UNSIGNED NOT NULL,
    type          ENUM('up','down','spam','rate') NOT NULL,
    value         INT(1)        NOT NULL Default 0,

    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

