SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `follows`;
CREATE TABLE IF NOT EXISTS `follows` (
    `user_id`        BIGINT(20)     UNSIGNED NOT NULL,
    `object_type`    VARCHAR(40)    NOT NULL,
    `object_id`      BIGINT(20)     UNSIGNED NOT NULL,
    `status`         ENUM('PENDING','ACCEPT','DENIED')  NOT NULL DEFAULT 'ACCEPT',
    `creation_date`  DATETIME       NOT NULL,
    `update_date`    DATETIME       NOT NULL,

    PRIMARY KEY (`user_id`,`object_type`, `object_id`)
) ENGINE=InnoDB;
