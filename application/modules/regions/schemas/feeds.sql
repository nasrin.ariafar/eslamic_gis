SET FOREIGN_KEY_CHECKS=0;
--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
CREATE TABLE IF NOT EXISTS `activities` (
    `id`            BIGINT(20)  UNSIGNED NOT NULL AUTO_INCREMENT,
    `actor_id`      BIGINT(20)  NOT NULL,
    `actor_type`    VARCHAR(50) NOT NULL,
    `verb`          VARCHAR(50) NOT NULL,
    `object_id`     BIGINT(20)  DEFAULT NULL,
    `object_type`   VARCHAR(40) DEFAULT NULL,
    `target_type`   VARCHAR(40) DEFAULT NULL,
    `target_id`     BIGINT(20)  DEFAULT NULL,
    `context_type`  VARCHAR(40) DEFAULT NULL,
    `context_id`    BIGINT(20)  DEFAULT NULL,
    `comment_count` INT(8)      NOT NULL DEFAULT '0',
    `privacy`       INT(8)      DEFAULT NULL,
    `creation_date` DATETIME    NOT NULL,
    `update_date`   DATETIME    NOT NULL,

    PRIMARY KEY (`id`)
) ENGINE=InnoDB;



DROP TABLE IF EXISTS `feeds`;
CREATE TABLE IF NOT EXISTS `feeds` (
    `id`             BIGINT(20)  UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id`        BIGINT(20)  UNSIGNED NOT NULL,
    `activity_id`    BIGINT(20)  UNSIGNED NOT NULL,
    `verb`           VARCHAR(50) NOT NULL,
    `object_id`      BIGINT(20)  UNSIGNED DEFAULT NULL,
    `object_type`    VARCHAR(40) DEFAULT NULL,
    `privacy`        INT(8)      NULL,
    `creation_date`  DATETIME    NOT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB ;



DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
    `id`             BIGINT(20)    UNSIGNED NOT NULL    AUTO_INCREMENT,
    `activity_id`    BIGINT(20)    UNSIGNED NOT NULL,
    `from_id`        BIGINT(20)    UNSIGNED NOT NULL,
    `to_id`          BIGINT(20)    UNSIGNED NOT NULL,
    `type`           VARCHAR(50)   NOT NULL,
    `target_type`    VARCHAR(40)   NOT NULL,
    `target_id`      BIGINT(20)    UNSIGNED NOT NULL,
    `context_type`   VARCHAR(40)   DEFAULT NULL,
    `context_id`     BIGINT(20)    UNSIGNED DEFAULT NULL,
    `seen`           TINYINT(1)    NOT NULL,   -- 0:Unseen, 1:Seen
    `creation_date`  DATETIME      NOT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;
