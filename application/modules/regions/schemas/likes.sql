SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
    `user_id`        BIGINT(20)   UNSIGNED NOT NULL,
    `object_type`    VARCHAR(40)  NOT NULL,
    `object_id`      BIGINT(20)   UNSIGNED NOT NULL,
    `creation_date`  DATETIME     NOT NULL,

    PRIMARY KEY (`user_id`,`object_type`, `object_id`)
) ENGINE=InnoDB;
