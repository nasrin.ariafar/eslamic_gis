<?php

class Places_Service_Place extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Places_Model_DbTable_Places();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Places_Model_Place $place = null)
    {

        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$place instanceof Places_Model_Place) {
            $place = new Places_Model_Place();
        }
        $place->fill($row);
//        $place->setNew(false);
        return $place;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'places'), array('*'));
        $cSelect->from(array('u' => 'places'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {

                    case 'or':
                        if (is_array($value)) {
                            $conds = array();
                            foreach ($value as $_key => $_value) {
                                switch ($_key) {
                                    case 'public':
//                                        $conds[] = "u.isPrivate like 0";
                                        $conds[] = 'isPrivate like 0 and typeId IN (' . implode(',', $value[$_key]) . ')';
                                        break;

                                    case 'private':
                                        $conds[] = 'typeId IN (' . implode(',', $value[$_key]) . ')';
                                        break;
                                }
                            }

                            $cond = implode(' OR ', $conds);
                            $select->where($cond);
                            $cSelect->where($cond);
                        }
                        break;

                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;

                    case 'types' :
                        $select->where('typeId IN (?)', $filter['types']);
                        $cSelect->where('typeId IN (?)', $filter['types']);
                        break;

                    case 'countries' :
                        $select->where('countryId IN (?)', $filter['countries']);
                        $cSelect->where('countryId IN (?)', $filter['countries']);
                        break;

                    case 'name':
                        $select->where("CONCAT(u.name, u.localName) LIKE '%$value%'");
                        $cSelect->where("CONCAT(u.name, u.localName) LIKE '%$value%'");
                        break;

                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $places = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new Places_Model_Place();
            $item->fill($row);
            $places[] = $item;
        }

        return $places;
    }

    public function save(Places_Model_Place $place)
    {
        if ($place->isNew()) {
            $data = $place->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $place);
            }
        } else {
            $id = $place->getId();
            $data = $place->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $place);
        }

        return false;
    }

    public function remove(Places_Model_Place $place)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $place->getId());
        $this->_table->delete($where);
    }

    public function removeByConditions($conditions = array())
    {
        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->delete($where);
    }

    public function removeByQuery($property, $query)
    {
        $this->_table->delete("$property IN ($query)");
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
