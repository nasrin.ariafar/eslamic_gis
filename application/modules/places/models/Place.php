<?php

class Places_Model_Place extends Tea_Model_Entity
{

    const TYPE_PUBLIC = 1;
    const TYPE_PRIVATE = 2;
//    -------------------------------
    const STATUS_INPROGRESS = 1;
    const STATUS_PUBLISHED = 2;
    const STATUS_REJECTED = 3;

    protected $_properties = array(
        'id' => NULL,
        'name' => NULL,
        'localName' => NULL,
        'description' => NULL,
        'countryId' => NULL,
        'typeId' => NULL,
        'latitude' => NULL,
        'longitude' => NULL,
        'photo' => NULL,
        'email' => NULL,
        'website' => NULL,
        'tell' => NULL,
        'status' => 1,
        'isPrivate' => NULL,
        'createBy' => NULL,
        'acceptBy' => NULL,
        'managerName' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'name' :
                case 'localName':
                case 'description' :
                case 'countryId' :
                case 'typeId' :
                case 'latitude' :
                case 'longitude' :
                case 'photo' :
                case 'email' :
                case 'website' :
                case 'tell' :
                case 'status' :
                case 'isPrivate' :
                case 'createBy' :
                case 'acceptBy':
                case 'managerName' :
                case 'creationDate' :
                case 'updateDate' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

