<?php

class Feeds_Api_NotificationsController extends Tea_Controller_Rest_Action
{
    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit    = $this->_getParam('limit', 10);
        $start    = $this->_getParam('start', 0);
        $orderBy  = $this->_getParam('order_by', 'creation_date');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $user     = $this->_currentUser;

        if (!$user instanceof Users_Model_User) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (User)");
            return;
        }

        $sort   = array($orderBy => $orderDir);
        $filter = array(
            'to_id' => $user->getId(),
            'seen'  => 0
        );

        $notifs = Feeds_Service_Notification::getInstance()
            ->getList($filter, $sort, $start, $unseen, 1000);

        if ($unseen < $limit) {
            unset($filter['seen']);
            $notifs = Feeds_Service_Notification::getInstance()
                ->getList($filter, $sort, $start, $count, $limit);
        }

        $list = array();
        foreach ($notifs as $notif) {
            $from = Feeds_Model_Objects_User::getItem($notif->getFromId());
            $to   = Feeds_Model_Objects_User::getItem($notif->getToId());

            if ($notif->getTargetType() != null) {
                $class = 'Feeds_Model_Objects_' . ucfirst(strtolower($notif->getTargetType()));
                if (is_callable($class . '::getItem')) {
                    $target = $class::getItem($notif->getTargetId());

                    $target['target_type'] = $notif->getTargetType();
                }
            }
            $context = null;
            switch ($notif->getContextType()) {
            case 'user':
                $context = Feeds_Model_Objects_User::getItem($notif->getContextId());
                $context['context_type'] = 'user';
                break;
            case 'site':
                $context = Feeds_Model_Objects_Site::getItem($notif->getContextId());
                $context['context_type'] = 'site';
                break;
            }

            $_notif = array(
                'id'            => $notif->getId(),
                'creation_date' => $notif->getCreationDate(),
                'seen'          => (bool)$notif->getSeen(),
                'type'          => $notif->getType(),
                'from'          => $from,
                'to'            => $to,
                'target'        => $target,
                'context'       => $context,
            );

            $list[] = $_notif;
        }

        $result = array(
            'unseen' => $unseen,
            'list'   => $list
        );

        $response = json_encode($result);
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody($response);
    }

    public function putAction()
    {
        $notificationService = Feeds_Service_Notification::getInstance();

        $req = $this->_getParam('id');
        if (strtolower($req) != 'seen') {
            $this->getResponse()
                ->setHttpResponseCode(400)
                ->appendBody("Bad Request");
            return;
        }

        if ($this->_currentUser == null) {
            $this->getResponse()
                ->setHttpResponseCode(401)
                ->appendBody("Unauthorized");
            return false;
        }

        $notificationService->seeNotifications($this->_currentUser);

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody("success");
    }
}
